/* Copyright 2014 Guilherme Pereira
 * rrtstar_field.h
 *
 *  Created on: Dec 09, 2015
 *      Author: Guilherme Pereira
 */

#include "rrtstar_field_navigation/rrtstar_field.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;

using namespace ca::rrtstar_field;
using namespace coro;


ob::Cost FollowVectorField::motionCost(const ob::State *state1, const ob::State *state2) const
  {
    const ob::RealVectorStateSpace::StateType *s1 = state1->as<ob::RealVectorStateSpace::StateType>();
    const ob::RealVectorStateSpace::StateType *s2 = state2->as<ob::RealVectorStateSpace::StateType>();	
    double x=s2->values[0]-s1->values[0]; 
    double y=s2->values[1]-s1->values[1];	
    vec v(x, y);
    double module=v.module();
    v.normalize();
    vec u=field(s1); // Assuming it is alredy normalized
    ob::Cost c((1-u.innerProduct(v))*module);
    return c;
    
  } // motionCost

  
  
og::PathGeometric FollowVectorField::integrateField(const ob::State *start, double delta, double max_path_size) 
  {
                 
	double xstart =start->as<ob::RealVectorStateSpace::StateType>()->values[0];
	double ystart =start->as<ob::RealVectorStateSpace::StateType>()->values[1];
			
	const ob::RealVectorStateSpace::StateType *current_state = start->as<ob::RealVectorStateSpace::StateType>();
	     
	og::PathGeometric field_path(si_, start);
	
	
	double path_size = 0.0;
	
	// Se a dostancia for maior que o raio de busca ou se o tamanho do caminho for maior que duas vezes o raio de busca
	// A inegral é finalizada.
	while ( path_size < max_path_size )
	{
	  path_size += delta; // Incrementa o path_size em delta.
	   
	  vec v=field(current_state);
	  v.normalize();
	  v.multiply(delta);
	
	  current_state->values[0]=current_state->values[0]+v.x;
	  current_state->values[1]=current_state->values[1]+v.y;
	  
	  if  (!si_->isValid(current_state)) { 				  // If the path is in collision...
	    OMPL_INFORM("Collision in the field integral!");
	    field_path.keepBefore(field_path.getState(0));                // ... clear the path. 
	    break;
	  }
	    
	  field_path.append(current_state);

	}
	
	current_state->values[0] = xstart;
	current_state->values[1] = ystart;
      
	return field_path;
       
  }  // integrateField
  

// ob::Cost FollowVectorField3::motionCost(const ob::State *state1, const ob::State *state2) const
//   {
//     const ob::RealVectorStateSpace::StateType *s1 = state1->as<ob::RealVectorStateSpace::StateType>();
//     const ob::RealVectorStateSpace::StateType *s2 = state2->as<ob::RealVectorStateSpace::StateType>();	
//     double x=s2->values[0]-s1->values[0]; 
//     double y=s2->values[1]-s1->values[1];	
//     double z=s2->values[2]-s1->values[2];
//     vec3 v(x, y, z);	
//     double module=v.module();
//     v.normalize();
//     vec3 u=field(s1); // Assuming it is alredy normalized
//     ob::Cost c((1-u.innerProduct(v))*module);
//     return c;
//   } // motionCost

ob::Cost FollowVectorField3::motionCost(const ob::State *state1, const ob::State *state2) const
  {
    const ob::RealVectorStateSpace::StateType *s1 = state1->as<ob::RealVectorStateSpace::StateType>();
    const ob::RealVectorStateSpace::StateType *s2 = state2->as<ob::RealVectorStateSpace::StateType>();	
    double x=s2->values[0]-s1->values[0]; 
    double y=s2->values[1]-s1->values[1];	
    double z=s2->values[2]-s1->values[2];
    vec3 v(x, y, z);	
    double module=v.module();
    v.normalize();
    vec3 u=field(s1); // Assuming it is alredy normalized
    //ob::Cost c((1-u.innerProduct(v))*module);
    
    double custo = (1-u.innerProduct(v))*module;
    
    
/*    //===================================================================================================
    // Aumenta o valor do custo proporcinalmente a aproximação ao obstáculo. Ou seja, quando a distância
    // é menor ou igual a 'offset_cost' de distância do obstáculo mais próximo, o custo é penalizado.
    
    double clearance_value =  si_->getStateValidityChecker()->clearance(state2); // Distância do obstáculo mais pŕoximo
    double offset_cost = 1.5; // Valor do offset do obstáculo em que o custo é penalizado.
    if( clearance_value < offset_cost )
      custo += (offset_cost - clearance_value); // Penalização proporcinal a aproximação ao obstáculo
    //===================================================================================================
  */    
    ob::Cost c(custo);
    return c;
  } // motionCost


  
  og::PathGeometric FollowVectorField3::integrateField(const ob::State *start, double delta, double max_path_size, bool collisionTest) 
  {
                 
	double xstart =start->as<ob::RealVectorStateSpace::StateType>()->values[0];
	double ystart =start->as<ob::RealVectorStateSpace::StateType>()->values[1];
	double zstart =start->as<ob::RealVectorStateSpace::StateType>()->values[2];
			
	const ob::RealVectorStateSpace::StateType *current_state = start->as<ob::RealVectorStateSpace::StateType>();
	
	
	
     
	og::PathGeometric field_path(si_, start);
	
	/*double distance = sqrt( (xstart-current_state->values[0])*(xstart-current_state->values[0]) + 
				(ystart-current_state->values[1])*(ystart-current_state->values[1]) + 
				(zstart-current_state->values[2])*(zstart-current_state->values[2]));*/
	          
	double path_size = 0.0;
	
	 OMPL_INFORM ( "(integrateField) current_state = %f, %f, %f", 
		    current_state->values[0], current_state->values[1], current_state->values[2] );
	
	
	while ( path_size < max_path_size )
	{
	  path_size += delta; // Incrementa o path_size em delta.
	   
	  vec3 v=field(current_state);
	  //v.normalize();
	  v.multiply(delta);

	  current_state->values[0]=current_state->values[0]+v.x;
	  current_state->values[1]=current_state->values[1]+v.y;
	  current_state->values[2]=current_state->values[2]+v.z;
	  
	  if(collisionTest)
	  {
	    if  (!si_->isValid(current_state))
	    { 				  // If the path is in collision...
	      OMPL_INFORM("Collision in the field integral!");
	      //field_path.keepBefore(field_path.getState(0));                // ... clear the path. 
	      field_path.clear();
	      break;
	    }	  
	  }
	  
	  
	  field_path.append(current_state);
	  	  
	  /*distance = sqrt( (xstart-current_state->values[0])*(xstart-current_state->values[0]) + 
			   (ystart-current_state->values[1])*(ystart-current_state->values[1]) + 
			   (zstart-current_state->values[2])*(zstart-current_state->values[2]));*/
	
	}
      
	current_state->values[0] = xstart;
	current_state->values[1] = ystart;
	current_state->values[2] = zstart;
      
	return field_path;
       
  }  // integrateField
  
  
bool RRTstarField::computeFinalPath() const
  {
     
       std::vector<Motion*> motions;
       Motion *startMotion = new Motion(si_); // Assuming a single Start state  
       double distanceFromStart;
       Motion *solution = new Motion(si_);	

       if (nn_)
           nn_->list(motions);
          
   	
       // This is to find the start position.
       for (std::size_t i = 0 ; i < motions.size() ; ++i)
       	{
	  if (motions[i]->parent == NULL){
		startMotion->state = motions[i]->state;
		break;
          }
	}	
	
       ob::Cost minimumCost = opt_->infiniteCost();
       std::size_t minimumCostIndex = motions.size()-1; // Changed uppon sugestion of Elias  
       bool did_not_find_gol=true;
       double delta_radius_local=delta_radius;
       do {
	  for (std::size_t i = 0 ; i < motions.size() ; ++i)
	  {
	  // Compute the minimimum cost node at the Ball Surface.  
	  distanceFromStart=si_->distance(motions[i]->state, startMotion->state);
	  if ((distanceFromStart > ball_radius-delta_radius_local) && (distanceFromStart < ball_radius+delta_radius_local)) 
		if (motions[i]->cost.value() < minimumCost.value()){
		   minimumCost = motions[i]->cost;
		   minimumCostIndex = i;
		   did_not_find_gol=false;
		}
	  }
       
	  delta_radius_local*=1.1; // Increment delta_radius to increase the probability of finding the goal
	  if (delta_radius_local>closeness_ratio*ball_radius) { // Give up if delta_radius is too big. Return a path with start.
	      OMPL_WARN("Did not find the goal! Do not trust the path! The number of samples may be too small!");
	      resetFoundPath();
	      og::PathGeometric *geoPath = new og::PathGeometric(si_);
	      geoPath->append(startMotion->state);
	      ob::PathPtr path(geoPath);
              // Add the solution path.
              ob::PlannerSolution psol(path);
              psol.setPlannerName(getName());
              pdef_->addSolutionPath(psol); 
	      return false;
	  }
      } while (did_not_find_gol); 
       
       solution = motions[minimumCostIndex]; // The goal is defined as the node of minimum cost at the Ball Surface.

      // Find the best path         
      if (solution != NULL)
       {
           
           // construct the solution path
           std::vector<Motion*> mpath;
           while (solution != NULL)
           {
               mpath.push_back(solution);
               solution = solution->parent;
           }
   
           // set the solution path
	   og::PathGeometric *geoPath = new og::PathGeometric(si_);
           for (int i = mpath.size() - 1 ; i >= 0 ; --i)
               geoPath->append(mpath[i]->state);
   
           ob::PathPtr path(geoPath);

           // Add the solution path.
           ob::PlannerSolution psol(path);
           psol.setPlannerName(getName());
           pdef_->addSolutionPath(psol);
          }
       setFoundPath();  
       return true;

  } // computeFinalPath


ob::StateSamplerPtr RealVectorStateSpaceField::allocDefaultStateSampler() const
  { 
	if (dimension_==2) 
	      return ob::StateSamplerPtr(new RealVectorStateSamplerCircular(this));
	else
	      return ob::StateSamplerPtr(new RealVectorStateSamplerCircular3(this));	
  } // allocDefaultStateSampler



// Samples inside the circle.
void RealVectorStateSamplerCircular::sampleUniform(ob::State *state)
   {     
       ob::RealVectorStateSpace::StateType *start = space_->as<RealVectorStateSpaceField>()->getStart();
       double ball_radius = space_->as<RealVectorStateSpaceField>()->getBallRadius();	
       ob::RealVectorStateSpace::StateType *rstate = static_cast<ob::RealVectorStateSpace::StateType*>(state);
       do{
       	   double radius = ball_radius*sqrt(rng_.uniform01());
           double phi = 2.0*M_PI*rng_.uniform01();
           rstate->values[0] = start->values[0]+radius*cos(phi);
           rstate->values[1] = start->values[1]+radius*sin(phi);
       } while (!space_->satisfiesBounds(rstate));		
   
   } // sampleUniform


// Samples inside the Ball in 3D.
void RealVectorStateSamplerCircular3::sampleUniform(ob::State *state)
   {     
       ob::RealVectorStateSpace::StateType *start = space_->as<RealVectorStateSpaceField>()->getStart();
       double ball_radius = space_->as<RealVectorStateSpaceField>()->getBallRadius();	
       ob::RealVectorStateSpace::StateType *rstate = static_cast<ob::RealVectorStateSpace::StateType*>(state);
       do{
	    double radius = ball_radius*pow(rng_.uniform01(), 1.0/3.0);
 	    double phi = 2.0*M_PI*rng_.uniform01(); 	
    	    double costheta=1.0-2.0*rng_.uniform01();
    	    double radsintheta = radius*sin(acos(costheta));
            rstate->values[0] = start->values[0]+radsintheta*cos(phi);
            rstate->values[1] = start->values[1]+radsintheta*sin(phi);
	    rstate->values[2] = start->values[2]+radius*costheta;
       } while (!space_->satisfiesBounds(rstate));	
 	 
   } // sampleUniform

