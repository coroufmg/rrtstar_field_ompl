//#include "../include/rrtstar_field_navigation/scan_ob.h"
//#include <../../opt/ros/indigo/include/sensor_msgs/LaserScan.h>

#include <tf/tf.h>

#include "rrtstar_field_navigation/scan_ob.h"
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PointStamped.h>

namespace ob = ompl::base;
namespace og = ompl::geometric;


/*
void transformPoint(const tf::TransformListener& listener){
  //we'll create a point in the base_laser frame that we'd like to transform to the base_link frame
  geometry_msgs::PointStamped laser_point;
  laser_point.header.frame_id = "base_laser";
  //we'll just use the most recent transform available for our simple example
  laser_point.header.stamp = ros::Time();
  //just an arbitrary point in space
  laser_point.point.x = 1.0;
  laser_point.point.y = 2.0;
  laser_point.point.z = 0.0;

  geometry_msgs::PointStamped base_point;
  listener.transformPoint("base_link", laser_point, base_point);
  ROS_INFO("base_laser: (%.2f, %.2f. %.2f) -----> base_link: (%.2f, %.2f, %.2f) at time %.2f",
  laser_point.point.x, laser_point.point.y, laser_point.point.z,
  base_point.point.x, base_point.point.y, base_point.point.z,
  base_point.header.stamp.toSec() );
  ROS_ERROR("Received an exception trying to transform a point from \"base_laser\" to \"base_link\": %s", ex.what());
}
*/
scan_ob::scan_ob ( const ob::SpaceInformationPtr &si, ros::NodeHandle &nh, std::string topic_name, bool debug ) :
    ob::StateValidityChecker ( si ), m_nh ( nh ), m_gotScan ( false ), m_debug ( debug )
{
    m_minDist = 1.0;
    m_maxDist = 10.0;
    m_stateRadius = 1.0;
    
    m_visionLimit = M_PI*0.5; // Pode variar de 0 a pi.
    m_droneAngle = 0;

    m_secure_minDist = 1.4; // em metros
    m_secure_liveTime = 0.4; // em segundos
    m_lastScanData = ros::Time::now();

    m_nh.param ( "/rrtstar_field/secure_initDist", m_secure_initDist, 0.3 );
    m_nh.param ( "/rrtstar_field/secure_minDist", m_secure_minDist, 1.4 );
    m_nh.param ( "/rrtstar_field/secure_liveTime", m_secure_liveTime, 0.4 );
    m_nh.param ( "/rrtstar_field/secure_enable", m_secure_enable, true );
    
    m_nh.param ( "/rrtstar_field/num_seq_laser_points", m_num_seq_laser_points, 50 );
    m_nh.param ( "/rrtstar_field/top_heigth_dist", m_top_heigth_dist, 0.60 );
    m_nh.param ( "/rrtstar_field/down_heigth_dist", m_down_heigth_dist, -0.40 );
    m_nh.param ( "/rrtstar_field/intersection_limit", m_intersection_limit, 0.90 );
    m_nh.param ( "/rrtstar_field/history_duration", m_history_duration, 10.0 );
    m_nh.param ( "/rrtstar_field/max_diff", m_max_diff, 0.5 );
    
    double vLimit = 90.0;
    m_nh.param ( "/rrtstar_field/visionLimit", vLimit, 90.0 );
    m_visionLimit = vLimit * M_PI / 180.0;

    //m_attitudeSub       = m_nh.subscribe("dji_sdk/attitude", 1, &M100::attitude_callback, this);
    m_scan_sub = m_nh.subscribe<sensor_msgs::LaserScan> ( topic_name, 1, &scan_ob::scanCallback, this );
    m_vis_pub = m_nh.advertise<visualization_msgs::MarkerArray> ( "vis_marker_circles", 0 );
}

bool scan_ob::isScanOk()
{
    if(!m_secure_enable)
      return true;
    
    ros::spinOnce();
  
    // Teste de tempo sem comunicação
    ros::Duration diffTime = ros::Time::now() - m_lastScanData;
    if ( diffTime.toSec() >= m_secure_liveTime )
    {
        ROS_ERROR ( "scan_ob::isScanOk(): Laser comunication fail!" );
        return false;
    }

    int numPts = m_scan_pts.ranges.size();
    if ( numPts < 10 )
    {
        ROS_ERROR ( "scan_ob::isScanOk(): Laser data fail!" );
        return false;
    }

    for ( int i = 0; i < numPts; i++ )
    {
        double distance = m_scan_pts.ranges[i];
        if ( distance > m_secure_initDist && distance < m_secure_minDist ) // distance > 0.1 apenas para funcionar na simulacao
        {

            bool simulation;
            m_nh.param ( "/path_follow/simulation", simulation, false );
            if ( ! ( simulation && distance <= 0.12 ) )
            {
                ROS_ERROR ( "scan_ob::isScanOk(): Laser minimal secure distance fail! obstacle distance = %f", distance );
                return false;
            }
        }
    }

    return true;
}



void scan_ob::adjustVel()
{

  // Percorre os pontos do laser e encontra a menor distancia
  double minDistance = 0;
  bool firstVal = true;
  for ( int i = 0; i < m_scan_pts.ranges.size(); i++ )
  {
    double distance = m_scan_pts.ranges[i];

    if( std::isfinite( distance ) )
    {
      if( distance > m_minDist && distance < m_maxDist )
      {
	if(firstVal)
	{
	  minDistance = distance;
	  firstVal = false;
	}
	else
	{
	  if( distance < minDistance )
	    minDistance = distance;
	}
      }
    }
  }
  
  if(m_debug)
    ROS_INFO ( "scan_ob::adjustVel(): \n minDistance = %f \n ", minDistance );  
  
  if( minDistance > 0.0 )
  {
    double distVelMin = m_stateRadius;
    double distVelMax, velMax, velMin, vel;
    m_nh.param ( "/rrtstar_field/distVelMax", distVelMax, m_stateRadius*3 );
    m_nh.param ( "/rrtstar_field/velMax", velMax, 1.1 );    
    m_nh.param ( "/rrtstar_field/velMin", velMin, 0.4 );
    
    if( minDistance <= distVelMin )
      vel = velMin;
    else if( minDistance >= distVelMax )
      vel = velMax;
    else
    {
      double velFactor = (minDistance - distVelMin) / (distVelMax - distVelMin);
      vel = (velMax-velMin)*velFactor + velMin;
    }
    
    m_nh.setParam ( "/rrtstar_field/droneVel", vel );
    
    if(m_debug)
      ROS_INFO ( "scan_ob::adjustVel(): vel = %f", vel );
  }
}


void scan_ob::scanCallback ( const sensor_msgs::LaserScan::ConstPtr &scan_in )
{
    m_scan_pts = *scan_in;

    m_lastScanData = ros::Time::now();

    if ( !isScanOk() )
    {
        m_nh.setParam ( "/emergency_stop", true );
    }
    
    //adjustVel();


    if ( !m_gotScan )
    {
        m_gotScan = true;

        int numPts = m_scan_pts.ranges.size();


        ROS_INFO ( "scan_ob::scanCallback: \n numPts = %d, \n angle_min = %f, \n angle_max = %f, \n angle_increment = %f,"
                   "\n time_increment = %f, \n scan_time = %f, \n range_min = %f, \n range_max = %f",
                   numPts,
                   m_scan_pts.angle_min*180.0/M_PI,
                   m_scan_pts.angle_max*180.0/M_PI,
                   m_scan_pts.angle_increment*180.0/M_PI,
                   m_scan_pts.time_increment,
                   m_scan_pts.scan_time,
                   m_scan_pts.range_min,
                   m_scan_pts.range_max );


        /*
        int index = 20;
        double angle = m_scan_pts.angle_min + index * m_scan_pts.angle_increment;
        double distance = m_scan_pts.ranges[index];

        ROS_INFO( "scan_ob::scanCallback: angle = %f, distance = %f", angle*180.0/M_PI, distance );

        if( distance > m_scan_pts.range_max )
          distance = m_scan_pts.range_max;

        if( distance < m_scan_pts.range_min )
          distance = m_scan_pts.range_min;

        ROS_INFO( "scan_ob::scanCallback: angle = %f, distance = %f", angle*180.0/M_PI, distance );
        */
    }
}

void scan_ob::pubVisCircles ( const std::vector<Circle> &circles, double lifetime ) const
{
    //visualization_msgs::MarkerArray visCircles(circles.size());
    visualization_msgs::MarkerArray visCircles;
    visCircles.markers.resize ( circles.size() *2 );

    static int seq_count = 0;
    seq_count++;

    visualization_msgs::Marker marker;
    marker.header.seq = seq_count;
    marker.header.frame_id = "world";
    marker.header.stamp = ros::Time::now();
    marker.ns = "coro";
    marker.type = visualization_msgs::Marker::CYLINDER;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.z = 1.5;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.lifetime = ros::Duration ( lifetime );
    if ( m_debug )
    {
	ROS_INFO ( "scan_ob::pubVisCircles: lifetime = %f, marker.lifetime = %f", lifetime, marker.lifetime.toSec() );
    }    

    marker.color.a = 0.40; // Don't forget to set the alpha!
    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    for ( int i = 0; i < circles.size(); i++ )
    {
        marker.id = i;
        marker.pose.position.x = circles[i].x;
        marker.pose.position.y = circles[i].y;
        marker.scale.x = 2.0 * circles[i].r;
        marker.scale.y = 2.0 * circles[i].r;
        marker.scale.z = 3.0;

        if ( m_debug )
        {
            ROS_INFO ( "scan_ob::pubVisCircles: X = %f, Y = %f, R = %f", circles[i].x, circles[i].y, circles[i].r );
        }

        visCircles.markers[i] = marker;
        //visCircles.markers.push_back(marker);
        //visCircles[i] = marker;
    }
    //only if using a MESH_RESOURCE marker type:
    //marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae";
    //vis_pub.publish( marker );


    //////////////////////////////////////////////////////
    // Ciclindros que consideram o crescimento do obstáculo
    //////////////////////////////////////////////////////

    marker.color.a = 0.15; // Don't forget to set the alpha!
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    for ( int i = 0; i < circles.size(); i++ )
    {
        marker.id = i + circles.size();
        marker.pose.position.x = circles[i].x;
        marker.pose.position.y = circles[i].y;
        marker.scale.x = 2.0 * ( circles[i].r + m_stateRadius );
        marker.scale.y = 2.0 * ( circles[i].r + m_stateRadius );
        marker.scale.z = 3.0;

//         if ( m_debug )
//         {
//             ROS_INFO ( "scan_ob::pubVisCircles: X = %f, Y = %f, R = %f", circles[i].x, circles[i].y, circles[i].r );
//         }

        visCircles.markers[i + circles.size()] = marker;
        //visCircles.markers.push_back(marker);
        //visCircles[i] = marker;
    }

    m_vis_pub.publish ( visCircles );
}


/*
Circle scan_ob::testCollision( double x, double y, double &dist )
{

  Circle result;
  dist = 0;
  std::vector<Circle> m_circles;

  double shorterDistance = -1;
  for( int i = 0; i < m_circles.size(); i++ )
  {
    if( m_circles[i].collided(x,y) )
    {
      double dist = Circle(x,y).distance(m_circles[i]);
      if( shorterDistance == -1 )
      {
	result = m_circles[i];
	shorterDistance = dist;
      }

      if( dist < shorterDistance )
      {
	shorterDistance = dist;
	result = m_circles[i];
      }
    }
  }

  return shorterDistance;
}*/

std::vector<Circle> scan_ob::getObstacleCircles ( tf::TransformListener &listener )
{
    geometry_msgs::PointStamped laser_point, world_point, drone_point; //, world_drone;
    
    // Inicialização dos parâmetros
    laser_point.header.frame_id = "laser0_frame";
    laser_point.point.z = 0.0;
    laser_point.header.stamp = ros::Time();
    drone_point = laser_point;

    ros::spinOnce();
    
    int numPts = m_scan_pts.ranges.size();

    //if(m_debug)
    ROS_INFO( "scan_ob::getObstacleCircles: numPts = %d, = START =", numPts );
    
    double mav_heigth;
    m_nh.param ( "/rrtstar_field/mav_heigth", mav_heigth, 3.0 );    

    bool seqStart = false; // Inicio de uma sequecia
    bool insertCircle = false;
    Circle circle;
    int indexStart = -1;
    int indexEnd   = -1;

    double prev_distance = 0.0;
    if(numPts>0)
      prev_distance = m_scan_pts.ranges[0]; // Inicializa com o primeiro ponto
    
    // Percorre os pontos do laser
    for ( int i = 0; i < numPts; i++ )
    {
        double distance = m_scan_pts.ranges[i];
	if( !std::isfinite( distance ) )
	  distance = 1000.0;
	
	double dist_diff = distance - prev_distance; // Faz a derivada dos pontos
	prev_distance = distance;

	// Considera apenas pontos num range próximo ao robô
	
	// henrique -> testar top_heigth_dist
	// -> testa apenas pontos entre +-1 metro de altura em relação a altura atual de 3 metros
	// -> colocar os 3 metros como parâmetro
	
// 	if(m_debug && dist_diff >= m_max_diff)
// 	  ROS_INFO( "scan_ob::getObstacleCircles: i = %d \n dist_diff = %f \n distance = %f",
// 		    i, dist_diff, distance );
	  
	
	// Teste para inicio de uma sequencia de pontos
        if ( distance >= m_minDist && distance <= m_maxDist && fabs(dist_diff) < m_max_diff ) 
        {
            if ( !seqStart )
            {
                indexStart = i;
                seqStart = true;
		
/*		if(m_debug )
		  ROS_INFO( "scan_ob::getObstacleCircles:\n\n ####### SEQUENCIA START ########## \n" 
		  "i = %d \n dist_diff = %f \n distance = %f \n\n",
			    i, dist_diff, distance );     */ 		
            }
            
/*	  if(m_debug )
	    ROS_INFO( "scan_ob::getObstacleCircles: # SEQUENCIA # i = %d \n" 
	    " dist_diff = %f \n distance = %f", i, dist_diff, distance );   */         

            // Se tiver uma sequencia grande de pontos, ele quebra e
            // considera um novo objeto.
            if ( ( i-indexStart ) > m_num_seq_laser_points ) // henrique -> definir 50 como parametro
            {
	      insertCircle = true;
	      
/*	      if(m_debug )
		ROS_INFO( "scan_ob::getObstacleCircles: ####### SEQUENCIA END ########## \n" 
		"m_num_seq_laser_points LIMIT \n i = %d \n dist_diff = %f \n distance = %f \n\n",
			  i, dist_diff, distance );     */       
	      
            }
        }
        else
	{
	  if ( seqStart )
	  {
	    insertCircle = true;
/*	    if(m_debug )
	      ROS_INFO( "scan_ob::getObstacleCircles: ####### SEQUENCIA END ########## \n" 
	      "FORA DAS RESTRICOES \n i = %d \n dist_diff = %f \n distance = %f \n\n",
			i, dist_diff, distance );     */       
	    
	  }	  
	}

	if ( insertCircle )
	{
	    insertCircle = false;
	    indexEnd = i-1;

	    circle = getCircle ( indexStart,indexEnd );
	    
	    //if ( m_debug && circle.r > 1.5 )
		//ROS_INFO ( "\n\nscan_ob::getObstacleCircles(): \n #### CIRCULO DE RAIO GRANDE ### \n" 
		//"indexStart =  %d \n indexEnd = %d \n raio =  %.2f\n\n", indexStart, indexEnd, circle.r );

	    laser_point.point.x = circle.x;
	    laser_point.point.y = circle.y;

	    try
	    {
		listener.transformPoint ( "world", laser_point, world_point );
		if ( m_debug )
		    ROS_INFO ( "laser0_frame: (%.2f, %.2f. %.2f) -----> world: (%.2f, %.2f, %.2f) at time %.2f",
				laser_point.point.x, laser_point.point.y, laser_point.point.z,
				world_point.point.x, world_point.point.y, world_point.point.z,
				world_point.header.stamp.toSec() );		
		    
		/*listener.transformPoint ( "world", drone_point, world_drone );
		if ( m_debug )
		    ROS_INFO ( "drone_point: (%.2f, %.2f. %.2f) -----> world_drone: (%.2f, %.2f, %.2f) at time %.2f",
				drone_point.point.x, drone_point.point.y, drone_point.point.z,
				world_drone.point.x, world_drone.point.y, world_drone.point.z,
				world_drone.header.stamp.toSec() );		*/
		   
	    }
	    catch ( tf::TransformException ex )
	    {
		ROS_ERROR ( "%s",ex.what() );
		ros::Duration ( 1.0 ).sleep();
	    }
	    
	    double diffZ = world_point.point.z - mav_heigth;
	    if( diffZ > m_down_heigth_dist && 
	        diffZ < m_top_heigth_dist )
	    {
	      circle.x = world_point.point.x;
	      circle.y = world_point.point.y;
	      //circle.z = world_point.point.z;
	      circle.time_stamp = ros::Time::now();
// 	      
	      clearOldCircles();
	      
	      if( intersectionTest(circle) )
		m_circles.push_back ( circle ); // henrique -> testar interseção antes de adicionar circulo
	    }
	    else
	    {
	      if( m_debug )
		ROS_INFO( "scan_ob::getObstacleCircles(): A altura do obstaculo esta fora do limite de interesse. Altura relativa = %f", diffZ );
	    }
	    

	    //ROS_INFO( "scan_ob::getObstacleCircles: indexStart = %d, indexEnd = %d", indexStart, indexEnd );
	    indexStart = i;
	    seqStart = false;
	}
    }
    //if(m_debug)
    //ROS_INFO( "scan_ob::getObstacleCircles: numCircles = %d, = END =", (int)circles.size() );

    ROS_INFO ( "scan_ob::getObstacleCircles(): numCircles = %d, numPts = %d", ( int ) m_circles.size(), numPts );

    //ROS_INFO( "scan_ob::getObstacleCircles: numPts = %d, numCircles = %d", numPts, (int)circles.size() );

    //pubVisCircles(circles);
    //m_circles = circles;

    return m_circles;
}


bool scan_ob::intersectionTest(Circle circle)
{
  double max_inter = 0;
  double max_index = 0;
  
  double raioCirculo = circle.r;
  
  circle.r += m_stateRadius;
  
  for( int i = 0; i < m_circles.size(); i++ )
  {
    Circle circle_i = m_circles[i];
    circle_i.r += m_stateRadius;

    double inter = circle.intersection(circle_i);
    if( inter > max_inter )
    {
      max_inter = inter;
      max_index = i;
    }
  }
  
  if(m_debug)
    ROS_INFO( "scan_on::intersectionTest(): max_intersection = %f ", max_inter );
  
  // Se existe interseção
  if( max_inter > m_intersection_limit )
  {
    // Se o circulo novo for maior que o circulo atual, remove o atual.
    if( raioCirculo > m_circles[max_index].r )
    {
      m_circles.erase(m_circles.begin()+max_index);

      if(m_debug)
	ROS_INFO( "scan_on::intersectionTest(): Circle REMOVED.");

      return true; // Insere o novo circulo
    }
    else
    {
      if(m_debug)
	ROS_INFO( "scan_on::intersectionTest(): Circle WITH INTERSECTION.");
      return false; // Não insere o novo circulo, pois tem interseção com circulo antigo.
    }
  }

  // Se não tem nenhuma interseção, returna true.
  return true;
}

void scan_ob::clearOldCircles()
{
  for( int i = 0; i < m_circles.size(); i++ )
  {
    if( ros::Time::now() - m_circles[i].time_stamp > ros::Duration(m_history_duration) )
    {
       m_circles.erase(m_circles.begin()+i);
      if(m_debug)
	ROS_INFO( "scan_on::clearOldCircles(): Circle %d REMOVED.",i);       
       i--; // Retorna o indice
    }
  }
  
}

Circle scan_ob::getCircle ( int iStart, int iEnd ) const
{
    Circle circle;

    if ( iStart < 0 || iEnd < 0 )
    {
        return circle;
    }

    if ( iStart == iEnd )
    {
        circle = calcPosition ( iStart );
        circle.r = 0.1;
        return circle;
    }

    Circle pos1 = calcPosition ( iStart );
    Circle pos2 = calcPosition ( iEnd );

    circle.x = ( pos1.x + pos2.x ) / 2.0;
    circle.y = ( pos1.y + pos2.y ) / 2.0;
    circle.r = pos1.distance ( pos2 ) / 2.0;

    return circle;
}


Circle scan_ob::calcPosition ( int index ) const
{
    double distance = m_scan_pts.ranges[index];
    //double angle = (-m_scan_pts.angle_min) - index * m_scan_pts.angle_increment;
    double angle = m_scan_pts.angle_min + index * m_scan_pts.angle_increment;
    Circle circle ( distance*cos ( angle ), distance*sin ( angle ) );
    //ROS_INFO( "scan_ob::calcPosition: index = %d, angle = %f, distance = %f, pos.x = %f, pos.y = %f",
    //    index, angle*180.0/M_PI, distance, circle.x, circle.y );

    return circle;
}


/*Circle scan_ob::calcPosition( int index ) const
{
  double distance = m_scan_pts.ranges[index];
  double angle = m_scan_pts.angle_min + index * m_scan_pts.angle_increment;
  return Circle( distance*cos(angle), distance*sin(angle) );
}*/

// Testa se o 'state' é válido
bool scan_ob::isValid ( const ompl::base::State *state ) const
{
  
  
  // Limita o angulo do RRT*
  
//     // Get the point value
//      const ob::RealVectorStateSpace::StateType* state2D =
//          state->as<ob::RealVectorStateSpace::StateType>();
//      double x = state2D->values[0];
//      double y = state2D->values[1];
//      
//     // Modifica o ponto para o  referencial do robô
//     double px = x - m_drone_pose.pose.position.x;
//     double py = y - m_drone_pose.pose.position.y;
//     
//     // Testa se os valores estão próximos de zero
//     if( !( fabs(px) < 0.01 && fabs(py) < 0.01 ) )
//     {
//       // Angulo do 'state' em relação ao robo
//       double ptAngle = atan2(py,px);
//       
//       // Angulo do 'state' em relação ao robo
//       double diffAng = fabs( ptAngle - m_droneAngle);
//       
//       // Corrige o angulo quando o valor estiver fora de +- 180 graus.
//       if( diffAng > M_PI )
// 	diffAng = fabs( diffAng - 2*M_PI );
//       
//       //if(m_debug)
//       //{
//       /*  ROS_INFO( "scan_ob::isValid:\n droneX = %f, droneY = %f\n X = %f, Y = %f",
// 		  m_drone_pose.pose.position.x, m_drone_pose.pose.position.y, x, y );
// 	ROS_INFO( "scan_ob::isValid: dx = %f, dy = %f\n ptAngle = %f, droneAngle = %f\n diffAng = %f \n\n",
// 		  px, py, ptAngle*180.0/M_PI, m_droneAngle*180.0/M_PI, diffAng*180.0/M_PI );
//       //}
//       */
//       
//       // Se o angulo for maior que o limite retorna que o estado é inválido.
//       if( diffAng > m_visionLimit )
// 	return false;
//     }
     
    return clearance ( state ) >= 0.0;


    /*
    float32 angle_min        # start angle of the scan [rad]
    float32 angle_max        # end angle of the scan [rad]
    float32 angle_increment  # angular distance between measurements [rad]

    float32 time_increment   # time between measurements [seconds] - if your scanner
                             # is moving, this will be used in interpolating position
                             # of 3d points
    float32 scan_time        # time between scans [seconds]

    float32 range_min        # minimum range value [m]
    float32 range_max        # maximum range value [m]

    float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
    float32[] intensities    # intensity data [device-specific units].  If your
                             # device does not provide intensities, please leave
                             # the array empty.
    */




// m_scan_pts.angle_min


// return true;
}
/*
double scan_ob::clearanceCalc(const ompl::base::State* state, int &indexNearestCircle) const
{
  const ob::RealVectorStateSpace::StateType* state2D =
      state->as<ob::RealVectorStateSpace::StateType>();
  double x = state2D->values[0];
  double y = state2D->values[1];

  state2D->values[0] = -55.5;

  double nearestDistance = 100.0;

  for( int i = 0; i < m_circles.size(); i++ )
  {
    double dist = Circle(x,y).distance(m_circles[i]) - m_circles[i].r - m_stateRadius;
    if( dist < nearestDistance || i == 0 )
    {
      nearestDistance = dist;
      indexNearestCircle = i;
    }
  }

  return nearestDistance;
}*/


double scan_ob::clearanceCalc ( Circle state, int &indexNearestCircle ) const
{
    double nearestDistance = 100.0;

    for ( int i = 0; i < m_circles.size(); i++ )
    {
        double dist = state.distance ( m_circles[i] ) - m_circles[i].r - m_stateRadius;
        if ( dist < nearestDistance || i == 0 )
        {
            nearestDistance = dist;
            indexNearestCircle = i;
        }
    }

    return nearestDistance;
}


// Dado um circulo, obtem um ponto da borda mais próximo do state
Circle scan_ob::bestValidState ( int indexCircle, const Circle &state ) const
{
    int numPoints = 20;
    int iCircle=0;
    Circle nearestPt, auxPt;

    double nearestDistance = 100.0;
    int count = 0;

    Circle circle = m_circles[indexCircle];

    if ( m_debug )
        ROS_INFO ( "scan_ob::bestValidState: circle-> X = %f, Y = %f, R = %f",
                   circle.x, circle.y, circle.r );

    for ( int i = 0; i < numPoints; i++ )
    {
        // Varia de 0 a 360
        double angle = 2 * M_PI * i / numPoints;

        auxPt.x = circle.x + cos ( angle ) * ( circle.r + m_stateRadius + 0.01 );
        auxPt.y = circle.y + sin ( angle ) * ( circle.r + m_stateRadius + 0.01 );

        // Obtem ponto mais proximo valido
        double nearDist = clearanceCalc ( auxPt,iCircle );
        //ROS_INFO( "scan_ob::bestValidState: auxPt-> X = %f, Y = %f, nearDist = %f, i = %d",
        //      auxPt.x, auxPt.y, nearDist, i );
        if ( nearDist >= 0.0 )
        {
            double dist = auxPt.distance ( state );
            if ( dist < nearestDistance || count == 0 )
            {
                nearestPt = auxPt;
                nearestDistance = dist;

                //ROS_INFO( "scan_ob::bestValidState: nearestPt-> X = %f, Y = %f, dist = %f, i = %d",
                //	  nearestPt.x, nearestPt.y, dist, i );

            }
            count++;
        }
    }

    return nearestPt;
}



double scan_ob::getBestValidState ( const ompl::base::State *state ) const
{
    int indexNearestCircle = 0;
    Circle statePt, bestValidPt;

    const ob::RealVectorStateSpace::StateType *state2D =
        state->as<ob::RealVectorStateSpace::StateType>();

    statePt.x = state2D->values[0];
    statePt.y = state2D->values[1];

    double nearestDistance = scan_ob::clearanceCalc ( statePt, indexNearestCircle );

    // Calcula o ponto mais próximo fora do obstáculo
    if ( nearestDistance <= 0 )
    {
        bestValidPt = bestValidState ( indexNearestCircle, statePt );
        if ( bestValidPt.x != 0 && bestValidPt.y != 0 )
        {
            state2D->values[0] = bestValidPt.x;
            state2D->values[1] = bestValidPt.y;
        }
        else
        {
            ROS_ERROR ( "scan_ob::getBestValidState: ESTADO VALIDO NAO ENCONTRADO." );
        }
    }

    return nearestDistance;
}


double scan_ob::clearance ( const ompl::base::State *state ) const
{
    Circle statePt, bestValidPt;
    int indexNearestCircle=0;

    const ob::RealVectorStateSpace::StateType *state2D =
        state->as<ob::RealVectorStateSpace::StateType>();

    statePt.x = state2D->values[0];
    statePt.y = state2D->values[1];

    return clearanceCalc ( statePt, indexNearestCircle );
}



/*
double scan_ob::clearance(const ompl::base::State* state) const
{
  int indexNearestCircle = 0;
  //return clearanceCalc(state,indexCircle);
  //return 0;

  //double stateRadius = 1.0;


  const ob::RealVectorStateSpace::StateType* state2D =
      state->as<ob::RealVectorStateSpace::StateType>();
  double x = state2D->values[0];
  double y = state2D->values[1];
  //double z = state2D->values[2];
 // ROS_INFO( "scan_ob::clearance: X = %f, Y = %f", x, y );


  //std::vector<Circle> circles = this->getObstacleCircles();
  // Se nearestDistance é menor que zero significa que tem colisão
  double nearestDistance = 100.0;
  for( int i = 0; i < m_circles.size(); i++ )
  {
    //double dist = Circle(x,y).distance(circles[i]);
    //double dist = Circle().distance(m_circles[i]) - m_circles[i].r;
    double dist = Circle(x,y).distance(m_circles[i]) - m_circles[i].r - m_stateRadius;
    if( dist < nearestDistance || i == 0 )
    {
      nearestDistance = dist;
      indexNearestCircle = i;
    }
  }

  // Calcula o ponto mais próximo fora do obstáculo
  if( nearestDistance <= 0 )
  {
    m_circles[indexNearestCircle]


  }



  //pubVisCircles(circles);

  //if(m_circles.size()>0 && nearestDistance < 1.9)
    //ROS_INFO( "scan_ob::clearance: n = %d, X = %f, Y = %f, menor_dist = %f", (int)m_circles.size(), x, y, nearestDistance );


  return nearestDistance;
}*/
