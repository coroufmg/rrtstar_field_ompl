//#include "../include/rrtstar_field_navigation/circles_3d.h"
//#include <../../opt/ros/indigo/include/sensor_msgs/LaserScan.h>

#include <tf/tf.h>

#include "rrtstar_field_navigation/circles_3d.h"
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PointStamped.h>
#include <rrtstar_field_navigation/Circles.h>
#include <rrtstar_field_navigation/Circle.h>
#include <std_msgs/Float32.h>

using namespace coro;


circles_3d::circles_3d ( ros::NodeHandle &nh, std::string topic_name ) :
     m_nh ( nh ), m_gotScan ( false ), m_funnel_angle(180.0),
     m_laser_height(0.0)
{
    m_minDist = 1.0;
    m_maxDist = 10.0;
    m_stateRadius = 1.0;
    
    m_visionLimit = M_PI*0.5; // Pode variar de 0 a pi.
    m_droneAngle = 0;

    m_secure_minDist = 1.4; // em metros
    m_secure_liveTime = 0.4; // em segundos
    m_lastScanData = ros::Time::now();

    m_nh.param ( "/rrtstar_field/secure_initDist", m_secure_initDist, 0.3 );
    m_nh.param ( "/rrtstar_field/secure_minDist", m_secure_minDist, 1.4 );
    m_nh.param ( "/rrtstar_field/secure_liveTime", m_secure_liveTime, 0.4 );
    m_nh.param ( "/rrtstar_field/secure_enable", m_secure_enable, true );
    
    m_nh.param ( "/rrtstar_field/num_seq_laser_points", m_num_seq_laser_points, 50 );
    m_nh.param ( "/rrtstar_field/top_heigth_dist", m_top_heigth_dist, 0.60 );
    m_nh.param ( "/rrtstar_field/down_heigth_dist", m_down_heigth_dist, -0.40 );
    m_nh.param ( "/rrtstar_field/intersection_limit", m_intersection_limit, 0.90 );
    m_nh.param ( "/rrtstar_field/history_duration", m_history_duration, 10.0 );
    m_nh.param ( "/rrtstar_field/max_diff", m_max_diff, 0.5 );
    
    m_nh.param ( "/rrtstar_field_circles/debug_mode", m_debug, true );
    
    double vLimit = 90.0;
    m_nh.param ( "/rrtstar_field/visionLimit", vLimit, 90.0 );
    m_visionLimit = vLimit * M_PI / 180.0;

    //m_attitudeSub       = m_nh.subscribe("dji_sdk/attitude", 1, &M100::attitude_callback, this);
    m_scan_sub = m_nh.subscribe<sensor_msgs::LaserScan> ( topic_name, 1, &circles_3d::scanCallback, this );
    m_new_scan3d_sub = m_nh.subscribe( "/scan3d/new_scan", 1, &circles_3d::newScan3d, this );
    m_vis_pub = m_nh.advertise<visualization_msgs::MarkerArray> ( "vis_marker_circles", 0 );

    m_circles_pub = m_nh.advertise<rrtstar_field_navigation::Circles> ( "/circles_3d/circles", 0 );
    m_laser_height_pub = m_nh.advertise<std_msgs::Float32> ( "/circles_3d/laser_height", 0 );
    
    //m_scan_event = m_nh.advertise<std_msgs::Bool> ( "scan_event", 0 );
    
}

bool circles_3d::isScanOk()
{
    if(!m_secure_enable)
      return true;
  
    // Teste de tempo sem comunicação
    ros::Duration diffTime = ros::Time::now() - m_lastScanData;
    if ( diffTime.toSec() >= m_secure_liveTime )
    {
        ROS_ERROR ( "circles_3d::isScanOk(): Laser comunication fail!" );
        return false;
    }

    int numPts = m_scan_pts.ranges.size();
    if ( numPts < 10 )
    {
        ROS_ERROR ( "circles_3d::isScanOk(): Laser data fail!" );
        return false;
    }

    for ( int i = 0; i < numPts; i++ )
    {
        double distance = m_scan_pts.ranges[i];
        if ( distance > m_secure_initDist && distance < m_secure_minDist ) // distance > 0.1 apenas para funcionar na simulacao
        {

            bool simulation;
            m_nh.param ( "/path_follow/simulation", simulation, false );
            if ( ! ( simulation && distance <= 0.12 ) )
            {
                ROS_ERROR ( "circles_3d::isScanOk(): Laser minimal secure distance fail! obstacle distance = %f", distance );
                return false;
            }
        }
    }

    return true;
}



void circles_3d::adjustVel()
{

  // Percorre os pontos do laser e encontra a menor distancia
  double minDistance = 0;
  bool firstVal = true;
  for ( int i = 0; i < m_scan_pts.ranges.size(); i++ )
  {
    double distance = m_scan_pts.ranges[i];

    if( std::isfinite( distance ) )
    {
      if( distance > m_minDist && distance < m_maxDist )
      {
	if(firstVal)
	{
	  minDistance = distance;
	  firstVal = false;
	}
	else
	{
	  if( distance < minDistance )
	    minDistance = distance;
	}
      }
    }
  }
  
  if(m_debug)
    ROS_INFO ( "circles_3d::adjustVel(): \n minDistance = %f \n ", minDistance );  
  
  if( minDistance > 0.0 )
  {
    double distVelMin = m_stateRadius;
    double distVelMax, velMax, velMin, vel;
    m_nh.param ( "/rrtstar_field/distVelMax", distVelMax, m_stateRadius*3 );
    m_nh.param ( "/rrtstar_field/velMax", velMax, 1.1 );    
    m_nh.param ( "/rrtstar_field/velMin", velMin, 0.4 );
    
    if( minDistance <= distVelMin )
      vel = velMin;
    else if( minDistance >= distVelMax )
      vel = velMax;
    else
    {
      double velFactor = (minDistance - distVelMin) / (distVelMax - distVelMin);
      vel = (velMax-velMin)*velFactor + velMin;
    }
    
    m_nh.setParam ( "/rrtstar_field/droneVel", vel );
    
    if(m_debug)
      ROS_INFO ( "circles_3d::adjustVel(): vel = %f", vel );
  }
}

double circles_3d::getDistance( double angle ) const
{
  int index_range = 3;
  
  // Obtem o index do angulo desejado
  int index = int( (angle - m_scan_pts.angle_min) / m_scan_pts.angle_increment );
  
  int vec_tam = 2 * index_range + 1;
  std::vector<double> vec_h;
  
  Circle laser_pt;
  geometry_msgs::PointStamped laser_point, world_point;
  laser_point.header.frame_id = "laser0_frame";
  laser_point.point.z = 0.0;
  laser_point.header.stamp = ros::Time();

  for( unsigned int i = 0; i < vec_tam; ++i )
  {
    laser_pt = calcPosition ( index - index_range + i );
    laser_point.point.x = laser_pt.x;
    laser_point.point.y = laser_pt.y;

    try
    {
	m_listener.transformPoint ( "world", laser_point, world_point );
	vec_h.push_back(world_point.point.z);
    }
    catch(...)
    {
      return 0.0;
    }
//     catch ( tf::TransformException ex )
//     {
// 	ROS_ERROR ( "%s",ex.what() );
//     }
  }
  
  if(vec_h.empty())
    return 0.0;
  
  // Calcula a mediana do index +- index_range.
  
//   std::vector<double> vec_h( 
//     m_scan_pts.ranges.begin()+index-index_range,
//     m_scan_pts.ranges.begin()+index+index_range );
  
  std::sort(vec_h.begin(),vec_h.end());
  
  return( vec_h[vec_h.size()/2] );
  
  //return m_scan_pts.ranges[index];
}


void circles_3d::newScan3d ( const std_msgs::Bool::ConstPtr& new_scan )
{
  static ros::Time last_ime = ros::Time::now();
  ros::Duration diff_time = ros::Time::now() - last_ime;
  last_ime = ros::Time::now();
  
  
  double dt = diff_time.toSec();
  if(m_debug)
    ROS_INFO ( "circles_3d::newScan3d(): new_scan = %d, diff_time = %f", (int)(new_scan->data), dt );  
  if(dt < 0.3 )
    dt = 0.3;
  this->pubVisCircles(dt+0.5);
  
  std_msgs::Float32 laser_height; 
  
  
  if(new_scan->data) // -90 graus do servo motor 
    laser_height.data = getDistance(M_PI/2.0); // 90 graus do laser
  else // 90 graus do servo motor 
    laser_height.data = getDistance(-M_PI/2.0); // -90 graus do laser
    
  m_laser_height = laser_height.data;
  
  this->m_laser_height_pub.publish(laser_height);
}


void circles_3d::scanCallback ( const sensor_msgs::LaserScan::ConstPtr &scan_in )
{
    m_scan_pts = *scan_in;
//     std_msgs::Bool scan_evt;
//     scan_evt.data = true;
//     m_scan_event.publish(scan_evt);

    m_lastScanData = ros::Time::now();

    if ( !isScanOk() )
    {
        m_nh.setParam ( "/emergency_stop", true );
    }
    
    //adjustVel();


    if ( !m_gotScan )
    {
        m_gotScan = true;

        int numPts = m_scan_pts.ranges.size();


        ROS_INFO ( "circles_3d::scanCallback: \n numPts = %d, \n angle_min = %f, \n angle_max = %f, \n angle_increment = %f,"
                   "\n time_increment = %f, \n scan_time = %f, \n range_min = %f, \n range_max = %f",
                   numPts,
                   m_scan_pts.angle_min*180.0/M_PI,
                   m_scan_pts.angle_max*180.0/M_PI,
                   m_scan_pts.angle_increment*180.0/M_PI,
                   m_scan_pts.time_increment,
                   m_scan_pts.scan_time,
                   m_scan_pts.range_min,
                   m_scan_pts.range_max );

    }
    else
    {
      this->getObstacleCircles();
      
      //rrtstar_field_navigation::Circle rrt_circle;
      rrtstar_field_navigation::Circles rrt_circles;
      rrt_circles.circles.resize(m_circles.size());
      for( unsigned int i = 0; i < m_circles.size(); i++ )
      {
//  	rrt_circle.x = m_circles[i].x;
//  	rrt_circle.y = m_circles[i].y;
//  	rrt_circle.z = m_circles[i].z;
//  	rrt_circle.r = m_circles[i].r;
// 	
// 	rrt_circles.circles.push_back(rrt_circle);

 	rrt_circles.circles[i].x = m_circles[i].x;
 	rrt_circles.circles[i].y = m_circles[i].y;
 	rrt_circles.circles[i].z = m_circles[i].z;
 	rrt_circles.circles[i].r = m_circles[i].r;
	
      }

      m_circles_pub.publish(rrt_circles);
      
//       static int count = 0;
//       ++count;
//       if( count > 30 )
//       {
// 	count = 1;
// 	this->pubVisCircles(1.0);
//       }
    }
}

void circles_3d::pubVisCircles ( const std::vector<Circle> &circles, double lifetime ) const
{
    visualization_msgs::MarkerArray visCircles;
    //visCircles.markers.resize ( circles.size() *2 );
    visCircles.markers.resize ( circles.size() );

    static int seq_count = 0;
    seq_count++;

    visualization_msgs::Marker marker;
    marker.header.seq = seq_count;
    marker.header.frame_id = "world";
    marker.header.stamp = ros::Time::now();
    marker.ns = "coro";
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;
    //marker.pose.position.z = 1.5;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.lifetime = ros::Duration ( lifetime );
//     marker.color.a = 0.40; // Don't forget to set the alpha!
//     marker.color.r = 1.0;
//     marker.color.g = 0.0;
//     marker.color.b = 0.0;
//     for ( int i = 0; i < circles.size(); i++ )
//     {
//         marker.id = i;
//         marker.pose.position.x = circles[i].x;
//         marker.pose.position.y = circles[i].y;
//         marker.pose.position.z = circles[i].z;
//         marker.scale.x = 2.0 * circles[i].r;
//         marker.scale.y = 2.0 * circles[i].r;
//         marker.scale.z = 2.0 * circles[i].r;
// 
//         if ( m_debug )
//         {
//             ROS_INFO ( "circles_3d::pubVisCircles: X = %f, Y = %f, Z = %f, R = %f",
// 		       circles[i].x, circles[i].y, circles[i].z, circles[i].r );
//         }
// 5
//         visCircles.markers[i] = marker;
//     }
    //only if using a MESH_RESOURCE marker type:
    //marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae";
    //vis_pub.publish( marker );


    //////////////////////////////////////////////////////
    // Ciclindros que consideram o crescimento do obstáculo
    //////////////////////////////////////////////////////

    marker.color.a = 0.15; // Don't forget to set the alpha!
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    for ( int i = 0; i < circles.size(); i++ )
    {
        marker.id = i + circles.size();
        marker.pose.position.x = circles[i].x;
        marker.pose.position.y = circles[i].y;
        marker.pose.position.z = circles[i].z;
        marker.scale.x = 2.0 * ( circles[i].r + m_stateRadius );
        marker.scale.y = 2.0 * ( circles[i].r + m_stateRadius );
        marker.scale.z = 2.0 * ( circles[i].r + m_stateRadius );

//         if ( m_debug )
//         {
//             ROS_INFO ( "circles_3d::pubVisCircles: X = %f, Y = %f, R = %f", circles[i].x, circles[i].y, circles[i].r );
//         }

       // visCircles.markers[i + circles.size()] = marker;
        visCircles.markers[i] = marker;
	
    }
    
    ROS_INFO ( "circles_3d::pubVisCircles: size = %d", int(circles.size()) );

    m_vis_pub.publish ( visCircles );
}



std::vector<Circle> circles_3d::getObstacleCircles()
{
    geometry_msgs::PointStamped laser_point, world_point, drone_point; //, world_drone;
    
    // Inicialização dos parâmetros
    laser_point.header.frame_id = "laser0_frame";
    laser_point.point.z = 0.0;
    laser_point.header.stamp = ros::Time();
    drone_point = laser_point;

    ros::spinOnce();
    int numPts = m_scan_pts.ranges.size();

    if(m_debug)
      ROS_INFO( "circles_3d::getObstacleCircles: numPts = %d, = START =", numPts );
    
    double mav_heigth;
    m_nh.param ( "/rrtstar_field/mav_heigth", mav_heigth, 3.0 );    

    bool seqStart = false; // Inicio de uma sequecia
    bool insertCircle = false;
    Circle circle;
    int indexStart = -1;
    int indexEnd   = -1;

    double prev_distance = 0.0;
    if(numPts>0)
      prev_distance = m_scan_pts.ranges[0]; // Inicializa com o primeiro ponto
      
    clearOldCircles();
    
    // Percorre os pontos do laser
    for ( int i = 0; i < numPts; i++ )
    {
        double distance = m_scan_pts.ranges[i];
	if( !std::isfinite( distance ) )
	  distance = 1000.0;
	
	double dist_diff = distance - prev_distance; // Faz a derivada dos pontos
	prev_distance = distance;

	// Considera apenas pontos num range próximo ao robô
	
	// henrique -> testar top_heigth_dist
	// -> testa apenas pontos entre +-1 metro de altura em relação a altura atual de 3 metros
	// -> colocar os 3 metros como parâmetro
	
// 	if(m_debug && dist_diff >= m_max_diff)
// 	  ROS_INFO( "circles_3d::getObstacleCircles: i = %d \n dist_diff = %f \n distance = %f",
// 		    i, dist_diff, distance );
	  
	
	// Teste para inicio de uma sequencia de pontos
        if ( distance >= m_minDist && distance <= m_maxDist && fabs(dist_diff) < m_max_diff ) 
        {
            if ( !seqStart )
            {
                indexStart = i;
                seqStart = true;
		
/*		if(m_debug )
		  ROS_INFO( "circles_3d::getObstacleCircles:\n\n ####### SEQUENCIA START ########## \n" 
		  "i = %d \n dist_diff = %f \n distance = %f \n\n",
			    i, dist_diff, distance );     */ 		
            }
            
/*	  if(m_debug )
	    ROS_INFO( "circles_3d::getObstacleCircles: # SEQUENCIA # i = %d \n" 
	    " dist_diff = %f \n distance = %f", i, dist_diff, distance );   */         

            // Se tiver uma sequencia grande de pontos, ele quebra e
            // considera um novo objeto.
            if ( ( i-indexStart ) > m_num_seq_laser_points ) // henrique -> definir 50 como parametro
            {
	      insertCircle = true;
	      
/*	      if(m_debug )
		ROS_INFO( "circles_3d::getObstacleCircles: ####### SEQUENCIA END ########## \n" 
		"m_num_seq_laser_points LIMIT \n i = %d \n dist_diff = %f \n distance = %f \n\n",
			  i, dist_diff, distance );     */       
	      
            }
        }
        else
	{
	  if ( seqStart )
	  {
	    insertCircle = true;
/*	    if(m_debug )
	      ROS_INFO( "circles_3d::getObstacleCircles: ####### SEQUENCIA END ########## \n" 
	      "FORA DAS RESTRICOES \n i = %d \n dist_diff = %f \n distance = %f \n\n",
			i, dist_diff, distance );     */       
	    
	  }	  
	}

	if ( insertCircle )
	{
	    insertCircle = false;
	    indexEnd = i-1;

	    circle = getCircle ( indexStart,indexEnd );
	    
	    //if ( m_debug && circle.r > 1.5 )
		//ROS_INFO ( "\n\ncircles_3d::getObstacleCircles(): \n #### CIRCULO DE RAIO GRANDE ### \n" 
		//"indexStart =  %d \n indexEnd = %d \n raio =  %.2f\n\n", indexStart, indexEnd, circle.r );

	    laser_point.point.x = circle.x;
	    laser_point.point.y = circle.y;

	    try
	    {
		m_listener.transformPoint ( "world", laser_point, world_point );
// 		if ( m_debug )
// 		    ROS_INFO ( "laser0_frame: (%.2f, %.2f. %.2f) -----> world: (%.2f, %.2f, %.2f) at time %.2f",
// 				laser_point.point.x, laser_point.point.y, laser_point.point.z,
// 				world_point.point.x, world_point.point.y, world_point.point.z,
// 				world_point.header.stamp.toSec() );		
		    
	    }
	    catch ( tf::TransformException ex )
	    {
		ROS_ERROR ( "%s",ex.what() );
		ros::Duration ( 1.0 ).sleep();
	    }
	    
// 	    double diffZ = world_point.point.z - mav_heigth;
// 	    if( diffZ > m_down_heigth_dist && 
// 	        diffZ < m_top_heigth_dist )
// 	    {
	      circle.x = world_point.point.x;
	      circle.y = world_point.point.y;
	      circle.z = world_point.point.z;
	      circle.time_stamp = ros::Time::now();
	      
	      if(circle.z > m_laser_height + m_stateRadius ) // Só adiciona o circulo se estiver acima do solo
		if( intersectionTest(circle) )
		  m_circles.push_back ( circle ); // henrique -> testar interseção antes de adicionar circulo
// 	    }
// 	    else
// 	    {
// 	      if( m_debug )
// 		ROS_INFO( "circles_3d::getObstacleCircles(): A altura do obstaculo esta fora do limite de interesse. Altura relativa = %f", diffZ );
// 	    }
	    

	    //ROS_INFO( "circles_3d::getObstacleCircles: indexStart = %d, indexEnd = %d", indexStart, indexEnd );
	    indexStart = i;
	    seqStart = false;
	}
    }
    //if(m_debug)
    //ROS_INFO( "circles_3d::getObstacleCircles: numCircles = %d, = END =", (int)circles.size() );

    if(m_debug)
      ROS_INFO ( "circles_3d::getObstacleCircles(): numCircles = %d, numPts = %d", ( int ) m_circles.size(), numPts );

    //ROS_INFO( "circles_3d::getObstacleCircles: numPts = %d, numCircles = %d", numPts, (int)circles.size() );

    //pubVisCircles(circles);
    //m_circles = circles;

    return m_circles;
}


bool circles_3d::intersectionTest(Circle circle)
{
  double max_inter = 0;
  double max_index = 0;
  
  double raioCirculo = circle.r;
  
  circle.r += m_stateRadius;
  
  for( int i = 0; i < m_circles.size(); i++ )
  {
    Circle circle_i = m_circles[i];
    circle_i.r += m_stateRadius;

    double inter = circle.intersection(circle_i);
    if( inter > max_inter )
    {
      max_inter = inter;
      max_index = i;
    }
  }// 	      clearOldCircles();

  
  if(m_debug)
    ROS_INFO( "circles_3d::intersectionTest(): max_intersection = %f ", max_inter );
  
  // Se existe interseção
  if( max_inter > m_intersection_limit )
  {
    // Se o circulo novo for maior que o circulo atual, remove o atual.
    if( raioCirculo > m_circles[max_index].r )
    {
      m_circles.erase(m_circles.begin()+max_index);

      if(m_debug)
	ROS_INFO( "circles_3d::intersectionTest(): Circle REMOVED.");

      return true; // Insere o novo circulo
    }
    else
    {
      if(m_debug)
	ROS_INFO( "circles_3d::intersectionTest(): Circle WITH INTERSECTION.");
      
      // Atualiza time_stamp do circulo antigo com o time_stamp do circulo novo.
      m_circles[max_index].time_stamp = circle.time_stamp;
      
      return false; // Não insere o novo circulo, pois tem interseção com circulo antigo.
    }
  }

  // Se não tem nenhuma interseção, returna true.
  return true;
}

void circles_3d::clearOldCircles()
{
  if(m_debug)
    ROS_INFO( "circles_3d::clearOldCircles(): circles.size = %d ANTES", (int)m_circles.size()); 
  
  for( int i = 0; i < m_circles.size(); i++ )
  {
    ros::Duration dif_stamp = ros::Time::now() - m_circles[i].time_stamp;
    
    if(m_debug)
      ROS_INFO( "circles_3d::clearOldCircles(): Circle %d, dif_stamp = %f", i, dif_stamp.toSec() );       
    
    if( dif_stamp > ros::Duration(m_history_duration) )
    {
       m_circles.erase(m_circles.begin()+i);
      if(m_debug)
        ROS_INFO( "circles_3d::clearOldCircles(): Circle %d REMOVED.",i);       
       i--; // Retorna o indice
    }
  }
  if(m_debug)
    ROS_INFO( "circles_3d::clearOldCircles(): circles.size = %d DEPOIS", (int)m_circles.size()); 
  
}

Circle circles_3d::getCircle ( int iStart, int iEnd ) const
{
    Circle circle;

    if ( iStart < 0 || iEnd < 0 )
    {
        return circle;
    }

    if ( iStart == iEnd )
    {
        circle = calcPosition ( iStart );
        circle.r = 0.1;
        return circle;
    }

    Circle pos1 = calcPosition ( iStart );
    Circle pos2 = calcPosition ( iEnd );

    circle.x = ( pos1.x + pos2.x ) / 2.0;
    circle.y = ( pos1.y + pos2.y ) / 2.0;
    circle.r = pos1.distance ( pos2 ) / 2.0;

    return circle;
}


Circle circles_3d::calcPosition ( int index ) const
{
    double distance = m_scan_pts.ranges[index];
    //double angle = (-m_scan_pts.angle_min) - index * m_scan_pts.angle_increment;
    double angle = m_scan_pts.angle_min + index * m_scan_pts.angle_increment;
    Circle circle ( distance*cos ( angle ), distance*sin ( angle ) );
    //ROS_INFO( "circles_3d::calcPosition: index = %d, angle = %f, distance = %f, pos.x = %f, pos.y = %f",
    //    index, angle*180.0/M_PI, distance, circle.x, circle.y );

    return circle;
}



