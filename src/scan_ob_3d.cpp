//#include "../include/rrtstar_field_navigation/scan_ob_3d.h"
//#include <../../opt/ros/indigo/include/sensor_msgs/LaserScan.h>

#include <tf/tf.h>

#include "rrtstar_field_navigation/scan_ob_3d.h"
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PointStamped.h>

namespace ob = ompl::base;
namespace og = ompl::geometric;
using namespace coro;


scan_ob_3d::scan_ob_3d ( const ob::SpaceInformationPtr &si, ros::NodeHandle &nh, std::string topic_name ) :
    ob::StateValidityChecker ( si ), m_nh ( nh ), m_gotScan ( false ), m_funnel_angle(180.0),
    m_laser_height(0.0)
{
    m_minDist = 1.0;
    m_maxDist = 10.0;
    m_stateRadius = 1.0;
    
    m_visionLimit = M_PI*0.5; // Pode variar de 0 a pi.
    m_droneAngle = 0;

    m_secure_minDist = 1.4; // em metros
    m_secure_liveTime = 0.4; // em segundos

    m_nh.param ( "/rrtstar_field/secure_initDist", m_secure_initDist, 0.3 );
    m_nh.param ( "/rrtstar_field/secure_minDist", m_secure_minDist, 1.4 );
    m_nh.param ( "/rrtstar_field/secure_liveTime", m_secure_liveTime, 0.4 );
    m_nh.param ( "/rrtstar_field/secure_enable", m_secure_enable, true );
    
    m_nh.param ( "/rrtstar_field/num_seq_laser_points", m_num_seq_laser_points, 50 );
    m_nh.param ( "/rrtstar_field/top_heigth_dist", m_top_heigth_dist, 0.60 );
    m_nh.param ( "/rrtstar_field/down_heigth_dist", m_down_heigth_dist, -0.40 );
    m_nh.param ( "/rrtstar_field/intersection_limit", m_intersection_limit, 0.90 );
    m_nh.param ( "/rrtstar_field/history_duration", m_history_duration, 10.0 );
    m_nh.param ( "/rrtstar_field/max_diff", m_max_diff, 0.5 );
    
    m_nh.param ( "/scan_ob_3d/debug_mode", m_debug, true );    
    
    double vLimit = 90.0;
    m_nh.param ( "/rrtstar_field/visionLimit", vLimit, 90.0 );
    m_visionLimit = vLimit * M_PI / 180.0;

    //m_attitudeSub       = m_nh.subscribe("dji_sdk/attitude", 1, &M100::attitude_callback, this);
    m_scan_sub = m_nh.subscribe<rrtstar_field_navigation::Circles> ( topic_name, 1, &scan_ob_3d::circlesCallback, this );
    m_new_scan3d_sub = m_nh.subscribe( "/scan3d/new_scan", 1, &scan_ob_3d::newScan3d, this );
    //m_vis_pub = m_nh.advertise<visualization_msgs::MarkerArray> ( "vis_marker_circles", 0 );
    m_laser_height_sub = m_nh.subscribe<std_msgs::Float32> ( "/circles_3d/laser_height", 1, &scan_ob_3d::laserHeightCallback, this );
    
    m_scan_event = m_nh.advertise<std_msgs::Bool> ( "scan_event", 0 );
    
}
/*
void scan_ob_3d::adjustVel()
{

  // Percorre os pontos do laser e encontra a menor distancia
  double minDistance = 0;
  bool firstVal = true;
  for ( int i = 0; i < m_scan_pts.ranges.size(); i++ )
  {
    double distance = m_scan_pts.ranges[i];

    if( std::isfinite( distance ) )
    {
      if( distance > m_minDist && distance < m_maxDist )
      {
	if(firstVal)
	{
	  minDistance = distance;
	  firstVal = false;
	}
	else
	{
	  if( distance < minDistance )
	    minDistance = distance;
	}
      }
    }
  }
  
  if(m_debug)
    ROS_INFO ( "scan_ob_3d::adjustVel(): \n minDistance = %f \n ", minDistance );  
  
  if( minDistance > 0.0 )
  {
    double distVelMin = m_stateRadius;
    double distVelMax, velMax, velMin, vel;
    m_nh.param ( "/rrtstar_field/distVelMax", distVelMax, m_stateRadius*3 );
    m_nh.param ( "/rrtstar_field/velMax", velMax, 1.1 );    
    m_nh.param ( "/rrtstar_field/velMin", velMin, 0.4 );
    
    if( minDistance <= distVelMin )
      vel = velMin;
    else if( minDistance >= distVelMax )
      vel = velMax;
    else
    {
      double velFactor = (minDistance - distVelMin) / (distVelMax - distVelMin);
      vel = (velMax-velMin)*velFactor + velMin;
    }
    
    m_nh.setParam ( "/rrtstar_field/droneVel", vel );
    
    if(m_debug)
      ROS_INFO ( "scan_ob_3d::adjustVel(): vel = %f", vel );
  }
}*/


void scan_ob_3d::newScan3d ( const std_msgs::Bool::ConstPtr& new_scan )
{
  
    ROS_INFO ( "scan_ob_3d::newScan3d(): new_scan = %d", (int)(new_scan->data) );  
}


void scan_ob_3d::laserHeightCallback (const std_msgs::Float32::ConstPtr& laser_height)
{
  m_laser_height = laser_height->data;
}


void scan_ob_3d::circlesCallback (const rrtstar_field_navigation::Circles::ConstPtr& scan_in)
{
    m_rrt_circles = *scan_in;
    std_msgs::Bool scan_evt;
    scan_evt.data = true;
    m_scan_event.publish(scan_evt);

    //adjustVel();
    
    m_circles.clear();
    m_circles.resize(m_rrt_circles.circles.size());
    
    for( unsigned int i = 0; i < m_rrt_circles.circles.size(); i++ )
    {
      m_circles[i].x = m_rrt_circles.circles[i].x;
      m_circles[i].y = m_rrt_circles.circles[i].y;
      m_circles[i].z = m_rrt_circles.circles[i].z;
      m_circles[i].r = m_rrt_circles.circles[i].r;
    }


    if ( !m_gotScan )
    {
        m_gotScan = true;
    }
}


// Limita o angulo do RRT* num cone na direção do dst_point
bool scan_ob_3d::testFunnel(double *drone_state) const
{
  if( m_dst_point.x == 0 && m_dst_point.y == 0 )
    return true;

  coro::vec3 state_pt( 
    drone_state[0],
    drone_state[1],
    drone_state[2] );  

  
  // Modifica o ponto para o referencial do robô
  coro::vec3 vel = m_dst_point.dif(m_src_point);
  coro::vec3 pt = state_pt.dif(m_src_point);
  

  // Cálculo do ângulo entre dois vetores:  
  // cos(teta) = <vel,pt>/(|vel|.|pt|)
  double mod_vel = vel.module() * pt.module();
  if( mod_vel > 0.01 )
  {
    double teta = acos( vel.innerProduct(pt)/mod_vel ) * 180.0 / M_PI;
    
    // Se o angulo for menor que o limite, retorna que o estado é valido.
    return ( teta <= m_funnel_angle );
  }
  return true;
}

// Testa se o 'state' é válido
bool scan_ob_3d::isValid ( const ompl::base::State *state ) const
{
  
  
  // Limita o angulo do RRT*
  
//     // Get the point value
//      const ob::RealVectorStateSpace::StateType* state2D =
//          state->as<ob::RealVectorStateSpace::StateType>();
//      double x = state2D->values[0];
//      double y = state2D->values[1];
//      
//     // Modifica o ponto para o  referencial do robô
//     double px = x - m_drone_pose.pose.position.x;
//     double py = y - m_drone_pose.pose.position.y;
//     
//     // Testa se os valores estão próximos de zero
//     if( !( fabs(px) < 0.01 && fabs(py) < 0.01 ) )
//     {
//       // Angulo do 'state' em relação ao robo
//       double ptAngle = atan2(py,px);
//       
//       // Angulo do 'state' em relação ao robo
//       double diffAng = fabs( ptAngle - m_droneAngle);
//       
//       // Corrige o angulo quando o valor estiver fora de +- 180 graus.
//       if( diffAng > M_PI )
// 	diffAng = fabs( diffAng - 2*M_PI );
//       
//       //if(m_debug)
//       //{
//       /*  ROS_INFO( "scan_ob_3d::isValid:\n droneX = %f, droneY = %f\n X = %f, Y = %f",
// 		  m_drone_pose.pose.position.x, m_drone_pose.pose.position.y, x, y );
// 	ROS_INFO( "scan_ob_3d::isValid: dx = %f, dy = %f\n ptAngle = %f, droneAngle = %f\n diffAng = %f \n\n",
// 		  px, py, ptAngle*180.0/M_PI, m_droneAngle*180.0/M_PI, diffAng*180.0/M_PI );
//       //}
//       */
//       
//       // Se o angulo for maior que o limite retorna que o estado é inválido.
//       if( diffAng > m_visionLimit )
// 	return false;
//     }
     
    //return clearance ( state ) >= 0.0;
  
    const ob::RealVectorStateSpace::StateType *state3D =
	state->as<ob::RealVectorStateSpace::StateType>();  
	
    if( this->clearance ( state ) > 0.0 )
      return( testFunnel( state3D->values ) );


    /*
    float32 angle_min        # start angle of the scan [rad]
    float32 angle_max        # end angle of the scan [rad]
    float32 angle_increment  # angular distance between measurements [rad]

    float32 time_increment   # time between measurements [seconds] - if your scanner
                             # is moving, this will be used in interpolating position
                             # of 3d points
    float32 scan_time        # time between scans [seconds]

    float32 range_min        # minimum range value [m]
    float32 range_max        # maximum range value [m]

    float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
    float32[] intensities    # intensity data [device-specific units].  If your
                             # device does not provide intensities, please leave
                             # the array empty.
    */




// m_scan_pts.angle_min


    return false;
}

double scan_ob_3d::clearanceCalc ( Circle state, int &indexNearestCircle ) const
{
    double nearestDistance = 100.0;

    for ( int i = 0; i < m_circles.size(); i++ )
    {
        double dist = state.distance ( m_circles[i] ) - m_circles[i].r - m_stateRadius;
        if ( dist < nearestDistance || i == 0 )
        {
            nearestDistance = dist;
            indexNearestCircle = i;
        }
    }
    
    
    // Verifica a altura em relacao a altura do solo medido pelo laser
    //double z_limit = m_drone_pose.pose.position.z - m_laser_height;
    double distH = state.z -m_laser_height -m_stateRadius;
    
    return std::min( nearestDistance, distH );
}


// Dado um circulo, obtem um ponto da borda mais próximo do state
Circle scan_ob_3d::bestValidState ( int indexCircle, const Circle &state ) const
{
    int numPoints = 20;
    int iCircle=0;
    Circle nearestPt, auxPt;

    double nearestDistance = 100.0;
    int count = 0;

    Circle circle = m_circles[indexCircle];

    if ( m_debug )
        ROS_INFO ( "scan_ob_3d::bestValidState: circle-> X = %f, Y = %f, R = %f",
                   circle.x, circle.y, circle.r );

    for ( int i = 0; i < numPoints; i++ )
    {
        // Varia de 0 a 360
        double angle = 2 * M_PI * i / numPoints;

        auxPt.x = circle.x + cos ( angle ) * ( circle.r + m_stateRadius + 0.01 );
        auxPt.y = circle.y + sin ( angle ) * ( circle.r + m_stateRadius + 0.01 );

        // Obtem ponto mais proximo valido
        double nearDist = clearanceCalc ( auxPt,iCircle );
        //ROS_INFO( "scan_ob_3d::bestValidState: auxPt-> X = %f, Y = %f, nearDist = %f, i = %d",
        //      auxPt.x, auxPt.y, nearDist, i );
        if ( nearDist >= 0.0 )
        {
            double dist = auxPt.distance ( state );
            if ( dist < nearestDistance || count == 0 )
            {
                nearestPt = auxPt;
                nearestDistance = dist;

                //ROS_INFO( "scan_ob_3d::bestValidState: nearestPt-> X = %f, Y = %f, dist = %f, i = %d",
                //	  nearestPt.x, nearestPt.y, dist, i );

            }
            count++;
        }
    }

    return nearestPt;
}



double scan_ob_3d::getBestValidState ( const ompl::base::State *state ) const
{
    int indexNearestCircle = 0;
    Circle statePt, bestValidPt;

    const ob::RealVectorStateSpace::StateType *state2D =
        state->as<ob::RealVectorStateSpace::StateType>();

    statePt.x = state2D->values[0];
    statePt.y = state2D->values[1];

    double nearestDistance = scan_ob_3d::clearanceCalc ( statePt, indexNearestCircle );

    // Calcula o ponto mais próximo fora do obstáculo
    if ( nearestDistance <= 0 )
    {
        bestValidPt = bestValidState ( indexNearestCircle, statePt );
        if ( bestValidPt.x != 0 && bestValidPt.y != 0 )
        {
            state2D->values[0] = bestValidPt.x;
            state2D->values[1] = bestValidPt.y;
        }
        else
        {
            ROS_ERROR ( "scan_ob_3d::getBestValidState: ESTADO VALIDO NAO ENCONTRADO." );
        }
    }

    return nearestDistance;
}


double scan_ob_3d::clearance ( const ompl::base::State *state ) const
{
    Circle statePt, bestValidPt;
    int indexNearestCircle=0;

    const ob::RealVectorStateSpace::StateType *state2D =
        state->as<ob::RealVectorStateSpace::StateType>();

    statePt.x = state2D->values[0];
    statePt.y = state2D->values[1];
    statePt.z = state2D->values[2];

    return clearanceCalc ( statePt, indexNearestCircle );
}

