/* Copyright 2014 Guilherme Pereira
 * rrtstar_field.h
 *
 *  Created on: Dec 09, 2015
 *      Author: Guilherme Pereira
 *	Colaboration: Emylle Alves
 *
 */

#include "rrtstar_field_navigation/rrtstar_field.h"
#include "ompl/base/spaces/DubinsStateSpace.h"
namespace ob = ompl::base;
namespace og = ompl::geometric;

using namespace ca::rrtstar_field;


ob::Cost FollowVectorField::motionCost(const ob::State *state1, const ob::State *state2) const
  {
    const ob::DubinsStateSpace::StateType *s1 = state1->as<ob::DubinsStateSpace::StateType>();
    const ob::DubinsStateSpace::StateType *s2 = state2->as<ob::DubinsStateSpace::StateType>();	
    double x=s2->getX()-s1->getX(); 
    double y=s2->getY()-s1->getY();	
    vec v(x, y);
    double module=v.module();
    v.normalize();
    vec u=field(s1); // Assuming it is already normalized
    ob::Cost c((1-u.innerProduct(v))*module);
    return c;
    
  } // motionCost

  
  
og::PathGeometric FollowVectorField::integrateField(const ob::State *start, double delta, double radius_of_the_search_ball) 
  {
                 
	double xstart =start->as<ob::DubinsStateSpace::StateType>()->getX();
	double ystart =start->as<ob::DubinsStateSpace::StateType>()->getY();
			
	const ob::DubinsStateSpace::StateType *current_state = start->as<ob::DubinsStateSpace::StateType>();
	     
	og::PathGeometric field_path(si_, start);
	
	double distance = sqrt((xstart-current_state->getX())*(xstart-current_state->getX())+(ystart-current_state->getY())*(ystart-current_state->getY()));
	          
	while (distance<radius_of_the_search_ball)
	{
	   
	  vec v=field(current_state);
	  v.normalize();
	  v.multiply(delta);
	
	  current_state->getX(current_state.getX()+v.x);
	  current_state->getY()=ystart+v.y;
	  
	  if  (!si_->isValid(current_state)) { 				  // If the path is in collision...
	    OMPL_INFORM("Collision in the field integral!");
	    field_path.keepBefore(field_path.getState(0));                // ... clear the path. 
	    break;
	  }
	  
	  field_path.append(current_state);
	  	  
	  distance = sqrt((xstart-current_state->getX())*(xstart-current_state->getX())+(ystart-current_state->getY())*(ystart-current_state->getY()));
	
	}
      
	return field_path;
       
  }  // integrateField
  

// ob::Cost FollowVectorField3::motionCost(const ob::State *state1, const ob::State *state2) const
//   {
//     const ob::DubinsStateSpace::StateType *s1 = state1->as<ob::DubinsStateSpace::StateType>();
//     const ob::DubinsStateSpace::StateType *s2 = state2->as<ob::DubinsStateSpace::StateType>();	
//     double x=s2->getX()-s1->getX(); 
//     double y=s2->getY()-s1->getY();	
//     double z=s2->getYaw()-s1->getYaw();
//     vec3 v(x, y, z);	
//     double module=v.module();
//     v.normalize();
//     vec3 u=field(s1); // Assuming it is already normalized
//     ob::Cost c((1-u.innerProduct(v))*module);
//     return c;
//   } // motionCost


bool RRTstarField::computeFinalPath() const
  {
     
       std::vector<Motion*> motions;
       Motion *startMotion = new Motion(si_); // Assuming a single Start state  
       double distanceFromStart;
       Motion *solution = new Motion(si_);	

       if (nn_)
           nn_->list(motions);
          
   	
       // This is to find the start position.
       for (std::size_t i = 0 ; i < motions.size() ; ++i)
       	{
	  if (motions[i]->parent == NULL){
		startMotion->state = motions[i]->state;
		break;
          }
	}	
	
       ob::Cost minimumCost = opt_->infiniteCost();
       std::size_t minimumCostIndex = motions.size()-1; // Changed uppon sugestion of Elias  
       bool did_not_find_gol=true;
       double delta_radius_local=delta_radius;
       do {
	  for (std::size_t i = 0 ; i < motions.size() ; ++i)
	  {
	  // Compute the minimimum cost node at the Ball Surface.  
	  distanceFromStart=si_->distance(motions[i]->state, startMotion->state);
	  if ((distanceFromStart > ball_radius-delta_radius_local) && (distanceFromStart < ball_radius+delta_radius_local)) 
		if (motions[i]->cost.value() < minimumCost.value()){
		   minimumCost = motions[i]->cost;
		   minimumCostIndex = i;
		   did_not_find_gol=false;
		}
	  }
       
	  delta_radius_local*=1.1; // Increment delta_radius to increase the probability of finding the goal
	  if (delta_radius_local>closeness_ratio*ball_radius) { // Give up if delta_radius is too big. Return a path with start.
	      OMPL_WARN("Did not find the goal! Do not trust the path! The number of samples may be too small!");
	      resetFoundPath();
	      og::PathGeometric *geoPath = new og::PathGeometric(si_);
	      geoPath->append(startMotion->state);
	      ob::PathPtr path(geoPath);
              // Add the solution path.
              ob::PlannerSolution psol(path);
              psol.setPlannerName(getName());
              pdef_->addSolutionPath(psol); 
	      return false;
	  }
      } while (did_not_find_gol); 
       
       solution = motions[minimumCostIndex]; // The goal is defined as the node of minimum cost at the Ball Surface.

      // Find the best path         
      if (solution != NULL)
       {
           
           // construct the solution path
           std::vector<Motion*> mpath;
           while (solution != NULL)
           {
               mpath.push_back(solution);
               solution = solution->parent;
           }
   
           // set the solution path
	   og::PathGeometric *geoPath = new og::PathGeometric(si_);
           for (int i = mpath.size() - 1 ; i >= 0 ; --i)
               geoPath->append(mpath[i]->state);
   
           ob::PathPtr path(geoPath);

           // Add the solution path.
           ob::PlannerSolution psol(path);
           psol.setPlannerName(getName());
           pdef_->addSolutionPath(psol);
          }
       setFoundPath();  
       return true;

  } // computeFinalPath


ob::StateSamplerPtr DubinsStateSpaceField::allocDefaultStateSampler() const
  { 
	if (dimension_==2) 
	      return ob::StateSamplerPtr(new DubinsStateSamplerCircular(this));
	else
	      return ob::StateSamplerPtr(new DubinsStateSamplerCircular3(this));	
  } // allocDefaultStateSampler



// Samples inside the circle.
void DubinsStateSamplerCircular::sampleUniform(ob::State *state)
   {     
       ob::DubinsStateSpace::StateType *start = space_->as<DubinsStateSpaceField>()->getStart();
       double ball_radius = space_->as<DubinsStateSpaceField>()->getBallRadius();	
       ob::DubinsStateSpace::StateType *rstate = static_cast<ob::DubinsStateSpace::StateType*>(state);
       do{
       	   double radius = ball_radius*sqrt(rng_.uniform01());
           double phi = 2*M_PI*rng_.uniform01();
           rstate->setX(start->getX()+radius*cos(phi));
           rstate->setY(start->getY()+radius*sin(phi));
       } while (!space_->satisfiesBounds(rstate));		
   
   } // sampleUniform


// Samples inside the Ball in 3D.
void DubinsStateSamplerCircular3::sampleUniform(ob::State *state)
   {     
       ob::DubinsStateSpace::StateType *start = space_->as<DubinsStateSpaceField>()->getStart();
       double ball_radius = space_->as<DubinsStateSpaceField>()->getBallRadius();	
       ob::DubinsStateSpace::StateType *rstate = static_cast<ob::DubinsStateSpace::StateType*>(state);
       do{
	    double radius = ball_radius*pow(rng_.uniform01(), 1/3);
 	    double phi = 2*M_PI*rng_.uniform01(); 	
    	    double costheta=1-2*rng_.uniform01();
    	    double radsintheta = radius*sin(acos(costheta));
            rstate->getX() = start->getX() + radsintheta*cos(phi);
            rstate->getY() = start->getY() + radsintheta*sin(phi);
	    rstate->getYaw() = start->getYaw() + radius*costheta;
       } while (!space_->satisfiesBounds(rstate));	
 	 
   } // sampleUniform

