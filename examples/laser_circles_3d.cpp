/* Copyright 2019 Henrique Nunes Machado
 * laser_circles_3d.cpp
 *
 *  Created on: Jan 30, 2019 
 *      Author: Henrique Nunes Machado
 */

#include <ros/ros.h>

#include "rrtstar_field_navigation/circles_3d.h"

using namespace coro;

int laser_3d()
{
    ros::NodeHandle n;

    // Parameter of the problem
    double radius_of_the_search_ball, eta, squareArea, resolution, solveTime, droneVel, start_offset;
    double minObjDist, maxObjDist, droneRadius;
    bool publishMarkers, integrateField, debug_mode, laser_enable;

    n.param ( "/rrtstar_field/radius_of_the_search_ball", radius_of_the_search_ball, 5.0 );
    n.param ( "/rrtstar_field/eta", eta, 1.0 );
    n.param ( "/rrtstar_field/squareArea", squareArea, 15.0 );
    n.param ( "/rrtstar_field/resolution", resolution, 0.2 );

    n.param ( "/rrtstar_field/minObjDist", minObjDist, 1.0 );
    n.param ( "/rrtstar_field/maxObjDist", maxObjDist, 7.5 );
    n.param ( "/rrtstar_field/droneRadius", droneRadius, 2.0 );

    n.param ( "/rrtstar_field/solveTime", solveTime, 0.6 );
    n.param ( "/rrtstar_field/droneVel", droneVel, 1.0 );
    n.param ( "/rrtstar_field/start_offset", start_offset, 0.0 );
    n.param ( "/rrtstar_field/publishMarkers", publishMarkers, true );
    n.param ( "/rrtstar_field/integrateField", integrateField, true );
    
    n.param ( "/rrtstar_field/debug_mode", debug_mode, true );
    n.param ( "/rrtstar_field/laser_enable", laser_enable, true );


//     ROS_INFO("params: radius_of_the_search_ball = %f, eta = %f, squareArea = %f, resolution = %f, solveTime = %f, droneVel = %f",
// 	   radius_of_the_search_ball, eta, squareArea, resolution, solveTime, droneVel);

    ROS_INFO ( "(laser_3d) INICIO." );

    circles_3d laserObstacle ( n,"/scan" );
   
    if(laser_enable) 
    {
      laserObstacle.setMinDist ( minObjDist );
      laserObstacle.setMaxDist ( maxObjDist );
      laserObstacle.setStateRadius ( droneRadius );
    }

    ros::spin();

    return 0;
  
}


int main ( int argc, char **argv )
{
    ROS_INFO("### (laser_3d) INIT ###");
	
    ros::init ( argc, argv, "laser_3d" );

    return laser_3d();
}






