#include <ros/ros.h>
//#include <tf/transform_listener.h>

#include "rrtstar_field_navigation/rrtstar_field.h"
#include "rrtstar_field_navigation/scan_ob.h"
#include "ompl_rviz/ompl_rviz.h"
 
#include <nav_msgs/Path.h>

namespace ob = ompl::base;
using namespace ca::rrtstar_field;

//nav_msgs::Path m_path;
tf::TransformListener *listener;
scan_ob *laserObstacle; 
void pathCallback(const nav_msgs::Path::ConstPtr& path_in)
{
  //m_path = *path_in;
  laserObstacle->getObstacleCircles((*listener));
  laserObstacle->pubVisCircles();
  
  ROS_INFO("(pub_vis_circles::pathCallback) MENSAGEM RECEBIDA.");
}


int main(int argc, char **argv) {

  bool debug_mode = true;
  double squareArea = 30;
  double radius_of_the_search_ball = 5.0;
  double minObjDist = 0.4;
  double maxObjDist = 6.0;
  double droneRadius = 2.5;
  
  ros::init(argc, argv, "pub_vis_circles");
  ros::NodeHandle n;

  ROS_INFO("(pub_vis_circles) INICIO.");
  
  listener = new tf::TransformListener();
  
  ob::StateSpacePtr space(new RealVectorStateSpaceField(2, radius_of_the_search_ball)); // the second parameter is the ball radius

  // Set the bounds of space   
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0, -squareArea); // x
  bounds.setLow(1, -squareArea); // y 
  bounds.setHigh(0, squareArea); // x
  bounds.setHigh(1, squareArea); // y
  space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

  // Construct a space information instance for this state space
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  
  /*n.param("/rrtstar_field/minObjDist", minObjDist, 1.0);
  n.param("/rrtstar_field/maxObjDist", maxObjDist, 7.5);
  n.param("/rrtstar_field/droneRadius", droneRadius, 2.0);  */
  laserObstacle = new scan_ob(si,n,"/scan",debug_mode);
  laserObstacle->setMinDist( minObjDist );
  laserObstacle->setMaxDist( maxObjDist );
  laserObstacle->setStateRadius( droneRadius );
  
  ros::Subscriber path_sub = n.subscribe<nav_msgs::Path>("/path_follow/path", 1, pathCallback);

  ros::spin();
}






