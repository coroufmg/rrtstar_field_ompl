/* Copyright 2014 Guilherme pereira
 * rrtstar_field_moving_square.cpp
 *
 *  Created on: Dec 16, 2015 (Updated to remove ca_stacks on April 20, 2017)
 *      Author: Guilherme Pereira
 * Modified by: Henrique Nunes Machado (January, 2018)
 */

//#define HECTOR_SIMU

#include <ros/ros.h>
//#include <tf/transform_listener.h>

#include "rrtstar_field_navigation/rrtstar_field.h"
#include "rrtstar_field_navigation/scan_ob_3d.h"
#include "ompl_rviz/ompl_rviz.h"

#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <ompl/geometric/PathSimplifier.h>

#include <ompl/geometric/planners/cforest/CForest.h>

//#include <sensor_msgs/LaserScan.h>
//#include <sensor_msgs/PointCloud.h>
//#include <laser_geometry/laser_geometry.h>

// Simulação do DJI M100 a aprtir do simulador Hector Quadrotor

#ifdef HECTOR_SIMU
#include <dji_m100/m100_hector.h>
#else
#include <dji_m100/m100.h>
#endif

namespace ob = ompl::base;
namespace og = ompl::geometric;

using namespace ca::rrtstar_field;
using namespace coro;


//========================================
// Dados para criar o funil limit do RRT*.
// ----
// dst_point - Ponto da direção do funil em relação a posição atual do drone.
coro::vec3 src_point(0,0,0);
coro::vec3 dst_point(0,0,0);
// funnel_angle - Angulo de abertura do funil.
double funnel_angle;
//========================================


/*geometry_msgs::Point toEulerAngle(geometry_msgs::Quaternion quat)
{
  geometry_msgs::Point ans;

  tf::Matrix3x3 R_FLU2ENU(tf::Quaternion(quat.x, quat.y, quat.z, quat.w));
  R_FLU2ENU.getRPY(ans.x, ans.y, ans.z);
  return ans;
}*/


// Callback para esperar o um sinal de start do nó 'path_follow_mav' que
// executa após o drone ter decolado e estar pronto para executar o algoritmo.
std_msgs::Bool wasStarted;
void WasStarted ( const std_msgs::Bool::ConstPtr &msg )
{
    wasStarted = *msg;
}



// Limita o angulo do RRT* num cone na direção do dst_point
bool testFunnel(double *drone_state)
{
  if( dst_point.x == 0 && dst_point.y == 0 )
    return true;

  vec3 state_pt( 
    drone_state[0],
    drone_state[1],
    drone_state[2] );  

  
  // Modifica o ponto para o referencial do robô
  vec3 vel = dst_point.dif(src_point);
  vec3 pt = state_pt.dif(src_point);
  

  // Cálculo do ângulo entre dois vetores:  
  // cos(teta) = <vel,pt>/(|vel|.|pt|)
  double mod_vel = vel.module() * pt.module();
  if( mod_vel > 0.01 )
  {
    double teta = acos( vel.innerProduct(pt)/mod_vel ) * 180.0 / M_PI;
    
    // Se o angulo for menor que o limite, retorna que o estado é valido.
    return ( teta <= funnel_angle );
  }
  return true;
}
  
    
// Define um obstáculo - Esfera 3D
class ValidityChecker : public ob::StateValidityChecker
{
public:
    ValidityChecker ( const ob::SpaceInformationPtr &si ) :
        ob::StateValidityChecker ( si ) {}
    // Returns whether the given state's position overlaps the
    // circular obstacle
    bool isValid ( const ob::State *state ) const
    {
        const ob::RealVectorStateSpace::StateType *state3D =
            state->as<ob::RealVectorStateSpace::StateType>();
	    
	// Extract the robot's (x,y) position from its state
        double z = state3D->values[2];
	
	if( this->clearance ( state ) > 0.0 && z > 0.5 )
	  return( testFunnel(state3D->values) );
	   
	return false;
      
        //return this->clearance ( state ) > 0.0;
	//return ( this->clearance ( state ) > 0.0 && z > 0.5 );
    }
    // Returns the distance from the given state's position to the
    // boundary of the circular obstacle.
    double clearance ( const ob::State *state ) const
    {
        // We know we're working with a RealVectorStateSpace in this
        // example, so we downcast state into the specific type.
        const ob::RealVectorStateSpace::StateType *state3D =
            state->as<ob::RealVectorStateSpace::StateType>();
        // Extract the robot's (x,y) position from its state
        double x = state3D->values[0];
        double y = state3D->values[1];
        double z = state3D->values[2];
        // Distance formula between two points, offset by the circle's
        // radius
        //return sqrt((x-5.5)*(x-5.5) + (y-0.1)*(y-0.1)) - 2.5; // Circle of radius 10 centered at (0.5,0.5)
        return sqrt ( ( x-5.5 ) * ( x-5.5 ) + ( y-0.0 ) * ( y-0.0 ) + ( z-3.0 ) * ( z-3.0 ) ) - 4.0; // Circle of radius 10 centered at (0.5,0.5)
    }
};


// Campo potencial em forma de um corredor
// Redefinition of FollowVectorField cost function to follow a corridor
class FollowVectorFieldCorridor : public FollowVectorField
{
public:
    FollowVectorFieldCorridor ( const ob::SpaceInformationPtr &si ) : FollowVectorField ( si ) {}

    vec field ( const ob::State *state ) const
    {
        double d0=5;
        double a=0.1;
        const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
        vec v ( 1, a* ( d0-s->values[1] ) );
        v.normalize();
        return v;
    }
};


// Campo potencial em forma de uma circulação quadrada
// Redefinition of FollowVectorField cost function to follow a square circulation

// CORRIGIR -> FAZER UM VECTOR FIELD EM 3D QUE CONVIRJA PARA UMA ALTURA Z ESPECÍFICA

class FollowVectorFieldCirculation : public FollowVectorField
{
public:
    FollowVectorFieldCirculation ( const ob::SpaceInformationPtr &si ) : FollowVectorField ( si ) {}

    vec field ( const ob::State *state ) const
    {
        const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
        double x=s->values[0];
        double y=s->values[1];

        double x3=x*x*x;
        double y3=y*y*y;
        double fi=x3*x+y3*y-1000.0;
        vec gradfi ( 4.0*x3, 4.0*y3 );
        vec gradHfi ( -gradfi.y, gradfi.x );
        //double G = -0.636619772367581*atan(10*fi); // atan(x*fi) - quanto maior o valor de x, converge mais rápido.
        double G = -0.636619772367581*atan ( 1000*fi );
        double H = -sqrt ( 1.0-G*G );
        double h = 1.0/gradfi.module();

        gradfi.normalize();
        gradfi.multiply ( 0.001*G );
        gradHfi.multiply ( H*h );

        vec u=gradfi.sum ( gradHfi );
        u.normalize();
        return u;
    }
};



class FollowVectorFieldCirculation3 : public FollowVectorField3
{
public:
    FollowVectorFieldCirculation3 ( const ob::SpaceInformationPtr &si ) : FollowVectorField3 ( si ) {}

    vec3 field ( const ob::State *state ) const
    {
        const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
        double x=s->values[0]; 
        double y=s->values[1];
        double z=s->values[2];


	double x3=x*x*x;
        double y3=y*y*y;

	double fi1 = x3*x + y3*y - 1000.0;
	double fi2 = z - 3.0; // Altura em relação ao solo

	vec3 gradfi1 ( 4.0*x3, 4.0*y3, 0 );
	vec3 gradfi2 ( 0, 0, 1 );

	vec3 gfi1_vec_gfi2 = gradfi1.cross(gradfi2);
	gfi1_vec_gfi2.normalize();
	
	double ganhoZ = 100000;
	double P = fi1*fi1 + ganhoZ * fi2*fi2;
	vec3 gradP = gradfi1;
	gradP.multiply( 2.0*fi1 );
	
	vec3 gradfi2_mult = gradfi2;
	gradfi2_mult.multiply( ganhoZ * 2.0 * fi2 );
	gradP = gradP.sum( gradfi2_mult );
	gradP.normalize();
	
	double G = -0.636619772367581*atan ( 1000*sqrt(P) );
        double H = -sqrt ( 1.0-G*G );

	vec3 u(0,0,0);
	
	gradP.multiply(G);
	u = u.sum(gradP);

	gfi1_vec_gfi2.multiply(H);
	u = u.sum(gfi1_vec_gfi2);

        return u;
    }
};

class FollowVectorFieldCirculation2 : public FollowVectorField3
{
public:
    FollowVectorFieldCirculation2 ( const ob::SpaceInformationPtr &si ) : FollowVectorField3 ( si ) {}

    vec3 field ( const ob::State *state ) const
    {
        const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
        double x=s->values[0];
        double y=s->values[1];
	double z=s->values[2];

        double x3=x*x*x;
        double y3=y*y*y;
        double fi=x3*x+y3*y-1000.0;
        vec gradfi ( 4.0*x3, 4.0*y3 );
        vec gradHfi ( -gradfi.y, gradfi.x );
        //double G = -0.636619772367581*atan(10*fi); // atan(x*fi) - quanto maior o valor de x, converge mais rápido.
        double G = -0.636619772367581*atan ( 1000*fi );
        double H = -sqrt ( 1.0-G*G );
        double h = 1.0/gradfi.module();

        gradfi.normalize();
        gradfi.multiply ( 0.001*G );
        gradHfi.multiply ( H*h );

        vec u=gradfi.sum ( gradHfi );
        u.normalize();
	
	
	// Controlador proporcinal em relação a altura h = 3.0
	double vz = (3.0-z)*0.4;
	
	
	vec3 u3( u.x, u.y, vz );
	u3.normalize();
	
        return u3;
    }
};


// // Define a função objetivo
// // This function is used to define the new objective which is to follow a square circulation
// ob::OptimizationObjectivePtr getFollowVectorFieldObjective ( const ob::SpaceInformationPtr &si )
// {
//     return ob::OptimizationObjectivePtr ( new FollowVectorFieldCirculation2 ( si ) );
// }


// Define a função objetivo
// This function is used to define the new objective which is to follow a square circulation
ob::OptimizationObjectivePtr getFollowVectorFieldObjective ( const ob::SpaceInformationPtr &si  )
{
    return ob::OptimizationObjectivePtr ( new FollowVectorFieldCirculation2 ( si ) );
}


double calcDist ( double x1, double y1, double z1, double x2, double y2, double z2 )
{
    double dx = x2-x1;
    double dy = y2-y1;
    double dz = z2-z1;
    return sqrt ( dx*dx + dy*dy + dz*dz );
}






/*
// Corta o path calculado a partir distancia de 'start_offset' metros.
int prevPoint(og::PathGeometric &calc_path, geometry_msgs::PoseStamped drone_pose, double offset)
{
  int path_size = (int)calc_path.getStateCount();
  
  if ( path_size <= 0 )
    return 0;
  
    int kinit = 0;
    
    // Percorre os itens do path em ordem decrescente
    for ( int k = path_size-1; k >= 0; k-- )
    {
	aux_state = calc_path.getState ( k );
// 		if ( debug_mode )
// 		{
// 		  ROS_INFO ( "PASSO 6.3.1" );
// 		  ROS_INFO ( "(rrtstar_field) k = %d, aux_state = %f, %f, %f",
// 			      k, aux_state[0], aux_state[1], aux_state[2] );		  
// 		  ROS_INFO ( "(rrtstar_field) drone_pose = %f, %f, %f",
// 			      drone_pose.pose.position.x,
// 			      drone_pose.pose.position.y,
// 			      drone_pose.pose.position.z );
// 		}
// 		
	
	

	// Mede a distancia do drone em relacao a estado k
	double dist = calcDist ( drone_pose.pose.position.x,
				  drone_pose.pose.position.y,
				  drone_pose.pose.position.z,
				  aux_state[0], aux_state[1], aux_state[2] );


	// Encontra o valor de 'k' para a distancia for menor ou igual a estimada.
	if ( dist <= offset )
	{
	    start = aux_state;
	    if ( debug_mode )
		ROS_INFO ( "(rrtstar_field) k = %d, offset = %f, dist = %f, start_offset = %f",
			    k, offset, dist, start_offset );
		
	    kinit = k;
	    break;
	}
    }
    
    if ( debug_mode )
	ROS_INFO ( "PASSO 6.4" );            
    // Se kinit for maior que zero, corta o path a partir do valor de 'k_init'
    if( kinit > 0 )
    {
      calc_path.keepAfter( calc_path.getState ( kinit ) );
      if ( debug_mode )
	  ROS_INFO ( "(rrtstar_field) calc_path.keepAfter kinit = %d"
		      ", past_path_size = %d, path_size = %d"
		      ", pos_x = %f, pos_y = %f, pos_z = %f",
		      kinit, path_size, (int)calc_path.getStateCount(),
		      drone_pose.pose.position.x,
		      drone_pose.pose.position.y,
		      drone_pose.pose.position.z );	      
    }
	  
}
*/


int rrtstar_field_main()
{

    funnel_angle = 180.0;
    double dst_point_offset = 1.5;
    const int dim = 3; // Number of dimensions
 
    ros::NodeHandle n;

    // Parameter of the problem
    double radius_of_the_search_ball, eta, squareArea, resolution, solveTime, droneVel, start_offset;
    double minObjDist, maxObjDist, droneRadius;
    bool publishMarkers, integrateField, debug_mode, laser_enable;
    //int k_nearst = 0;

    n.param ( "/rrtstar_field/radius_of_the_search_ball", radius_of_the_search_ball, 5.0 );
    n.param ( "/rrtstar_field/eta", eta, 1.0 );
    n.param ( "/rrtstar_field/squareArea", squareArea, 15.0 );
    n.param ( "/rrtstar_field/resolution", resolution, 0.2 );

    /*n.param("/rrtstar_field/minObjDist", minObjDist, 1.0);
    n.param("/rrtstar_field/maxObjDist", maxObjDist, 7.5);
    n.param("/rrtstar_field/droneRadius", droneRadius, 2.0);

    n.param("/rrtstar_field/solveTime", solveTime, 0.6);
    n.param("/rrtstar_field/droneVel", droneVel, 1.0);
    n.param("/rrtstar_field/start_offset", start_offset, 0.0);
    n.param("/rrtstar_field/publishMarkers", publishMarkers, true);
    n.param("/rrtstar_field/integrateField", integrateField, true);*/
    n.param ( "/rrtstar_field/debug_mode", debug_mode, true );
    n.param ( "/rrtstar_field/laser_enable", laser_enable, true );


//     ROS_INFO("params: radius_of_the_search_ball = %f, eta = %f, squareArea = %f, resolution = %f, solveTime = %f, droneVel = %f",
// 	   radius_of_the_search_ball, eta, squareArea, resolution, solveTime, droneVel);


    ROS_INFO ( "(rrtstar_field) INICIO." );

    //=============
    // PATH FOLLOW
    //-------------

    // Start signal
    ros::Subscriber start_sub = n.subscribe<std_msgs::Bool> ( "/path_follow/start", 1, WasStarted );
    // Path publisher
    ros::Publisher path_pub = n.advertise<nav_msgs::Path> ( "/path_follow/path", 1 );
    // Speed publisher
    ros::Publisher speed_pub = n.advertise<std_msgs::Float32> ( "/path_follow/speed", 1 );

    nav_msgs::Path drone_path;
    std_msgs::Float32 speed;
    geometry_msgs::PoseStamped drone_pose;
    int num_points=100;
    //=============
    
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 1." );

    //tf::TransformListener listener;

    //================
    // RVIZ SHOW DATA
    //----------------
    ros::Publisher pub_path_marker = n.advertise<visualization_msgs::Marker> ( "/rrtstar_field/path", 1 );
    ros::Publisher pub_graph_marker = n.advertise<visualization_msgs::Marker> ( "/rrtstar_field/graph", 1 );
    ros::Publisher pub_path_limit_cicle = n.advertise<visualization_msgs::Marker> ( "/rrtstar_field/limit_cicle", 1 );
    ros::Publisher pub_graph_center = n.advertise<geometry_msgs::PointStamped> ( "/rrtstar_field/graph_center", 1 );
    //================

    // Init delay
    ros::Duration ( 1.0 ).sleep();

    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 2." );

    // Construct the robot state space in which we're planning. We're
    // planning in a subset of R^3.
    
    ob::StateSpacePtr space ( new RealVectorStateSpaceField ( dim, radius_of_the_search_ball ) ); // the second parameter is the ball radius

    // Set the bounds of space
    ob::RealVectorBounds bounds ( dim );
    
    bounds.setLow ( 0, -squareArea ); // x
    bounds.setLow ( 1, -squareArea ); // y
    bounds.setLow ( 2, -squareArea ); // z
    
    bounds.setHigh ( 0, squareArea ); // x
    bounds.setHigh ( 1, squareArea ); // y
    bounds.setHigh ( 2, squareArea ); // z
    
    
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 3." );
    
    
    space->as<ob::RealVectorStateSpace>()->setBounds ( bounds );

    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 3.1." );
    // Construct a space information instance for this state space
    ob::SpaceInformationPtr si ( new ob::SpaceInformation ( space ) );



    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 3.2." );    
    
    // Declara obstaculo do tipo laser

    scan_ob_3d laserObstacle ( si,n,"/circles_3d/circles" );
    si->setStateValidityChecker ( ob::StateValidityCheckerPtr ( &laserObstacle ) );

    // A máxima distancia em que o laser considera um obstaculo é 50% maior que o raio do rrt ball.
    //laserObstacle.setMaxDist( radius_of_the_search_ball * 1.5 );
    /*laserObstacle.setMinDist( minObjDist );
    laserObstacle.setMaxDist( maxObjDist );
    laserObstacle.setStateRadius( droneRadius );
    */
   
    // TESTE com obstáculo virtual
    // Set the object used to check which states in the space are valid
    //si->setStateValidityChecker(ob::StateValidityCheckerPtr(new ValidityChecker(si)));
    
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 3.3." );    


    si->setStateValidityCheckingResolution ( resolution );
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 3.4." );
    //si->setup();
    
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 4." );
    

    // Create a problem instance
    ob::ProblemDefinitionPtr pdef ( new ob::ProblemDefinition ( si ) );
//
    // Set the objective function
    pdef->setOptimizationObjective ( getFollowVectorFieldObjective ( si ) );

    // Set our robot's starting state
    ob::ScopedState<> start ( space ), goal ( space ), aux_state ( space ), bestNearStart ( space ), lastValidState ( space );
    start->as<ob::RealVectorStateSpace::StateType>()->values[0] = -5.0;
    start->as<ob::RealVectorStateSpace::StateType>()->values[1] = -5.0;
    start->as<ob::RealVectorStateSpace::StateType>()->values[2] = 0.0;

    goal->as<ob::RealVectorStateSpace::StateType>()->values[0] = 10.0;
    goal->as<ob::RealVectorStateSpace::StateType>()->values[1] = 10.0;
    goal->as<ob::RealVectorStateSpace::StateType>()->values[2] = 0.0;

    // Set the start and goal states
    pdef->setStartAndGoalStates ( start, goal ); // Start and Goal can be the same. This will not change the final tree

    space->as<RealVectorStateSpaceField>()->setStart ( start ); // This is necessary for the sampler.
    
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 5." );
    

// Cria um RRTStar multithreaded usando o CForest    
//      ompl::base::PlannerPtr rrtstarPlannerCforest( new ompl::geometric::CForest(si) );
//      rrtstarPlannerCforest->as<ompl::geometric::CForest>()->addPlannerInstances<RRTstarField>(6);
// 
//      rrtstarPlannerCforest->as<RRTstarField>()->setRange(eta);

    // Construct our optimizing planner using the RRTstar algorithm.
    RRTstarField *rrtstarPlanner = new RRTstarField ( si );
    rrtstarPlanner->setRange ( eta );
    rrtstarPlanner->setRewireFactor ( 1.01 );
    rrtstarPlanner->setGoalBias ( 0 );

    rrtstarPlanner->setBallRadius ( radius_of_the_search_ball, 0.05*radius_of_the_search_ball );

    //rrtstarPlanner->setNumSamplingAttempts(15);
    //rrtstarPlanner->setSampleRejection(false);
    
    
    ob::PlannerPtr optimizingPlanner ( rrtstarPlanner );
    
    //ob::PlannerPtr optimizingPlanner ( new ompl::geometric::CForest(si) );
// 
//      rrtstarPlannerCforest->as<RRTstarField>()->setRange(eta);    
    

    // Set the problem instance for our planner to solve
    optimizingPlanner->setProblemDefinition ( pdef );

    optimizingPlanner->setup();

    
    
    //std::vector<og::PathGeometric> path(n_paths, og::PathGeometric(si));
    //og::PathGeometric final_path ( si );
    og::PathGeometric calc_path ( si );
    FollowVectorFieldCirculation2 field ( si ); // This is necessary to compute the integral of the field
    
    og::PathSimplifier path_simplifier(si);
    
    
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 6." );
    

    //=======
    // DRONE
    //-------

#ifdef HECTOR_SIMU
    coro::M100_hector m100 ( n,true ); // Read only
#else
    coro::M100 m100 ( n,true ); // Read only
#endif

    ros::Rate start_rate ( 2 );
    ROS_INFO ( "(rrtstar_field) Waiting for isAutonomous signal!" );
    while ( ros::ok() )
    {
        ros::spinOnce();

        if ( m100.isAutonomous() )
        {
            if ( m100.initZeroPos() )
            {
                ROS_INFO ( "(rrtstar_field) initZeroPos!" );
                break;
            }
        }

        start_rate.sleep();
    }

    //=======
    
    if(debug_mode)
      ROS_INFO ( "(rrtstar_field) INICIO 7." );
    


    ros::Rate loop_rate ( 0.5 );
    while ( !wasStarted.data && ros::ok() ) // Wait for the path
    {
        ros::spinOnce();
        ROS_INFO ( "(rrtstar_field) Waiting for start signal!" );
        loop_rate.sleep();
    }


    //optimizingPlanner->as<ompl::geometric::CForest>()->addPlannerInstances<RRTstarField>(6);


    if ( debug_mode )
    {
        ROS_INFO ( "PASSO 1" );
    }
    // ros::Duration(5.0).sleep();

    //ros::Time t_begin = ros::Time::now();
    //ros::Time t_end = t_begin + ros::Duration(solveTime);
    //ros::Duration t_loop = ros::Duration(solveTime);
    
    
    ros::spinOnce();
    drone_pose.pose = m100.getCurrPos();
    // START POSITION
    start[0] = drone_pose.pose.position.x;
    start[1] = drone_pose.pose.position.y;
    start[2] = drone_pose.pose.position.z;
    src_point.x = drone_pose.pose.position.x;
    src_point.y = drone_pose.pose.position.y;
    src_point.z = drone_pose.pose.position.z;
    laserObstacle.setDronePose(drone_pose);
    
    //final_path.append(start->as<ob::State>());


    //for (int i=0; i<n_paths; i++){
    //bool firstLoop = true;
    while ( ros::ok() )
    {
	// Leitura dos parâmetros variáveis
        n.param ( "/rrtstar_field/minObjDist", minObjDist, 1.0 );
        n.param ( "/rrtstar_field/maxObjDist", maxObjDist, 7.5 );
        n.param ( "/rrtstar_field/droneRadius", droneRadius, 2.0 );

        n.param ( "/rrtstar_field/solveTime", solveTime, 0.6 );
        n.param ( "/rrtstar_field/droneVel", droneVel, 1.0 );
        n.param ( "/rrtstar_field/start_offset", start_offset, 0.0 );
        n.param ( "/rrtstar_field/publishMarkers", publishMarkers, true );
        n.param ( "/rrtstar_field/integrateField", integrateField, true );
	

	if(laser_enable) 
	{
	  laserObstacle.setMinDist ( minObjDist );
	  laserObstacle.setMaxDist ( maxObjDist );
	  laserObstacle.setStateRadius ( droneRadius );
	}

        //=========================
        // set new start position
        ros::spinOnce();

//         // Testa uma parada de emergencia se o laser não estiver funcionando adequadamente
// 	if(laser_enable) 
// 	{
// 	  if ( !laserObstacle.isScanOk() )
// 	  {
// 	      n.setParam ( "/emergency_stop", true );
// 	      ROS_ERROR ( "(rrtstar_field) EMERGENCY STOP!" );
// 	      ros::Duration ( solveTime ).sleep();
// 	      continue;
// 	  }
// //         }

        // Atualiza a lista de onstaculos detectados pelo laser.
        // Faz a leitura dos obstáculos pŕoximos ao drone
        //if(laser_enable) 
	//  laserObstacle.getObstacleCircles( listener );
        //ros::Duration ( 0.01 ).sleep();
        drone_pose.pose = m100.getCurrPos();


        //ros::spinOnce();

        // START POSITION
//         start[0] = drone_pose.pose.position.x;
//         start[1] = drone_pose.pose.position.y;
//         start[2] = drone_pose.pose.position.z;

	
	/*	laserObstacle.setDronePose(drone_pose);	*/	
	
	
	//double droneAngle = toEulerAngle(drone_pose.pose.orientation).z;

        if ( debug_mode )
        {
            ROS_INFO ( "(rrtstar_field) start = %f, %f, %f", start[0], start[1], start[2] );
        }

        
        
        
        /*
	    
//==================================
        // Corta o path calculado a partir distancia de 'start_offset' metros.
	    
	int path_size = (int)calc_path.getStateCount();

	if ( path_size > 0 )
        {
	  
	  ros::spinOnce();
	   drone_pose.pose = m100.getCurrPos();
	  
            // Estima a distancia que o drone vai percorrerstart
            double estimated_dist = droneVel * solveTime + start_offset;
	    
	    int kinit = 0;

	    // Percorre os itens do path em ordem decrescente
            for ( int k = path_size-1; k >= 0; k-- )
            {
                aux_state = calc_path.getState ( k );

		// Mede a distancia do drone em relacao a estado k
                double dist = calcDist ( drone_pose.pose.position.x,
                                         drone_pose.pose.position.y,
                                         drone_pose.pose.position.z,
                                         aux_state[0], aux_state[1], aux_state[2] );

		// Encontra o valor de 'k' para a distancia for menor ou igual a estimada.
                if ( dist <= estimated_dist )
                {
                    start = aux_state;
                    if ( debug_mode )
                        ROS_INFO ( "(rrtstar_field) k = %d, estimated_dist = %f, dist = %f, start_offset = %f",
                                   k, estimated_dist, dist, start_offset );
			
		    kinit = k;
                    break;
                }
            }
            
            integrateField
        }
//==================================	            
        */
        
        //////////////////////////////////
        // CALCULO FUNNEL
	src_point.x = start[0];
	src_point.y = start[1];
	src_point.z = start[2];
	
//  	drone_pose.pose.position.x = start[0];
//  	drone_pose.pose.position.y = start[1];
//  	drone_pose.pose.position.z = start[2];
 	laserObstacle.setDronePose(drone_pose);		
	
	// Define o dst_point baseado na integral do campo
	og::PathGeometric dst_path=field.integrateField(start->as<ob::State>(), 0.2, dst_point_offset, false);
	aux_state = dst_path.getState(dst_path.getStateCount()-1);
	dst_point.x = aux_state[0];
	dst_point.y = aux_state[1];
	dst_point.z = aux_state[2];
	laserObstacle.setDstPoint(dst_point);
	
	geometry_msgs::Twist drone_vel = m100.getVel();
	vec3 drone_vel_vec( 
	      drone_vel.linear.x,
	      drone_vel.linear.y,
	      drone_vel.linear.z );        
	
	
	double drone_vel_mod = drone_vel_vec.module();
	double vel_ratio = drone_vel_mod / droneVel;
	if( vel_ratio > 1.0 )
	  vel_ratio = 1.0;
	
	//funnel_angle = 30.0 + ( 1.0 - vel_ratio) * 150.0;
	//funnel_angle = 50.0 + ( 1.0 - vel_ratio) * 130.0;
	funnel_angle = 90.0 + ( 1.0 - vel_ratio) * 90.0;
	laserObstacle.setFunnelAngle(funnel_angle);
	
	//if ( debug_mode )
	  ROS_INFO( "(rrtstar_field) ## FUNNEL ##  \n src = %f, %f, %f \n dst = %f, %f, %f \n funnel_angle = %f",
		    src_point.x,
		    src_point.y,
		    src_point.z,
		    dst_point.x,
		    dst_point.y,
		    dst_point.z,
		    funnel_angle );        
        //////////////////////////////////
        

        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 2" );
        }


        ////////////////////////////////////////////////////////////////
        // Se a posição estimado do start estiver no obstáculo, calcula 
        // uma posição mais próxima do start fora do obstáculo.

        // Test if is a valid start position (out of obstacle).
        bool isValidState = si->isValid ( start->as<ob::State>() );
        if ( !isValidState )
        {
            ROS_INFO ( "===================" );
            ROS_INFO ( "POSICAO INVALIDA!!!" );
            ROS_INFO ( "===================" );

            ROS_INFO ( " start invalido = %f, %f, %f", start[0], start[1], start[2] );
	    
	    
	   //==========================================================================================
	   // Percorre os itens em ordem decrescente do ultimo path calculado até achar um ponto valido
	    
	    //int path_size = (int)final_path.getStateCount();
	    int path_size = (int)calc_path.getStateCount();
	    
	    if(debug_mode)
	      ROS_INFO ( "path_size = %d", path_size );
	    
	
	    if ( path_size > 0 )
	    {
		//int kinit = 0;

		// Percorre os itens do path em ordem decrescente
// 		bool startOk = false;
// 		if( k_nearst >= path_size )
// 		  k_nearst = path_size-1;

// 		if(debug_mode)
// 		  ROS_INFO ( "k_nearst = %d", k_nearst );
	      
/*		dst_point.x = 0;
		dst_point.y = 0;
		dst_point.z = 0;
		laserObstacle.setDstPoint(dst_point);    */     
		
// 		for ( int k = path_size-1; k >= 0; k-- )
// 		{
// 		    aux_state = calc_path.getState ( k );
// 		    if(debug_mode)
// 		      ROS_INFO ( "k %d, aux_state = %f, %f, %f", k, aux_state[0], aux_state[1], aux_state[2] );
// 		    
// 		    if ( si->isValid ( aux_state->as<ob::State>() ) )
// 		    {
// 		      start = aux_state;
// 		      ROS_INFO ( "START VALIDO ENCONTRADO K = %d", k );
// 		      ROS_INFO ( "START VALIDO ENCONTRADO = %f, %f, %f", start[0], start[1], start[2] );
// 		      
// 		      break;
// 		    }
// 		}
		
		
		double menorDist = 1000.0;
		
		for ( int k = 0; k < path_size; k++ )
		{
		    aux_state = calc_path.getState ( k );
		    
// 		    if(debug_mode)
// 		      ROS_INFO ( "k %d, aux_state = %f, %f, %f", k, aux_state[0], aux_state[1], aux_state[2] );
		    
		    if ( si->isValid ( aux_state->as<ob::State>() ) )
		    {
		      // Mede a distancia do drone em relacao a estado k
		      double dist = calcDist ( drone_pose.pose.position.x,
					      drone_pose.pose.position.y,
					      drone_pose.pose.position.z,
					      aux_state[0], aux_state[1], aux_state[2] );	
		      if(dist < menorDist)
		      {
			menorDist = dist;
			start = aux_state;
			if(debug_mode)
			  ROS_INFO ( "START menorDist = %f, k = %d, pt = %f, %f, %f", menorDist, k, start[0], start[1], start[2] );
		      }
		    }
		}		
	    }
//==================================		    
	    
	    isValidState = si->isValid ( start->as<ob::State>() );
	    if ( !isValidState )
	    {
	      ROS_INFO ( "START CONTINUA INVALIDO! " );
	      
	      si->searchValidNearby( start->as<ob::State>(), start->as<ob::State>(),
				    radius_of_the_search_ball/2.0, (unsigned int)1000);
	      
	      ROS_INFO ( " START VALIDO CALCULADO = %f, %f, %f", start[0], start[1], start[2] );

	      if ( !si->isValid ( start->as<ob::State>() ) )
	      {
		  ROS_INFO ( "bestNearStart is not a valid state." );
		  start = lastValidState;
		  
		  // Envia sinal de parada de emergencia		  
		  n.setParam ( "/emergency_stop", true );
		  ROS_ERROR ( "(rrtstar_field) EMERGENCY STOP!" );
		  ros::Duration ( solveTime ).sleep();
		  continue;
	      }
	    }
        }
        else
        {
            lastValidState = start;
        }

        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 3" );
        }

        // Set the start and goal states.
        // Start and Goal can be the same. This will not change the final tree
        pdef->setStartAndGoalStates ( start, goal ); 

	 // This is necessary for the sampler.
        space->as<RealVectorStateSpaceField>()->setStart ( start );
	
        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 4" );
        }

        // Calculate rrt solution during 'x' seconds
        // Maior tempo de processamento do loop.
        ob::PlannerStatus solved = optimizingPlanner->solve ( solveTime );
	ros::spinOnce();
        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 5" );
        }

        // Publica os a visualisão dos obstáculos só após calpub_path_limit_ciclecular o RRT
        //if(laser_enable) 
	//  laserObstacle.pubVisCircles(solveTime+start_offset);
        
        if ( solved )
        {
	    // Publica dados da árvore do RRT apenas para visualisão.
            if ( publishMarkers )
            {
                ob::PlannerData data ( si );
                optimizingPlanner->getPlannerData ( data );
                //pub_graph_marker.publish ( ompl_rviz::GetGraph ( data, 100, 0.05, 0, 0, 1, 0.3 ) );
		pub_graph_marker.publish ( ompl_rviz::GetGraph ( data, dim, 0.02, 0, 0, 1, 0.3 ) );
		
		// Centro do GRAPH do RRT*
		geometry_msgs::PointStamped startPt; //((double)start[0],(double)start[1],(double)start[2]);
		startPt.point.x = start[0];
		startPt.point.y = start[1];
		startPt.point.z = start[2];
		
		startPt.header.frame_id = "world";
		pub_graph_center.publish( startPt );
            }

            // Mostra o ciclo limite do campo
             if(publishMarkers)
             {
		og::PathGeometric field_complete_path=field.integrateField(start->as<ob::State>(), 0.2, 40.0, false);
		
		if ( debug_mode )
		    ROS_INFO ( "(rrtstar_field) limit_cicle.size() = %d",
				(int)field_complete_path.getStateCount() );		
		
		pub_path_limit_cicle.publish(ompl_rviz::GetMarker(field_complete_path, 0.03, 0, 1, 1, 1, dim));
		
             }

	    // This computes the path. If you do not run this, you have standard RRT* path
            if ( rrtstarPlanner->computeFinalPath() )
                if ( debug_mode )
                {
                    ROS_INFO ( "Path computed!" );
                }

            // This indicates if it found a path. This is important if the borders of the
            // searching radius are blocked.
            if ( rrtstarPlanner->foundPath() )
                if ( debug_mode )
                {
                    ROS_INFO ( "Found path!" );
                }

            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 6" );
            }

            // Mostra o path - DEUBG
            //pdef->getSolutionPath()->print(std::cout);
            calc_path.clear();
            calc_path=dynamic_cast<const og::PathGeometric & > ( *pdef->getSolutionPath() );
	    
	    //ROS_INFO ( "calc_path = \n" );
	    //calc_path.print(std::cout);
	    
            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 6.1" );
            }

            // Integrate the field. If the integral of the field do not collide, use it as a path.
            if ( integrateField )
            {
                og::PathGeometric field_path=field.integrateField ( start->as<ob::State>(), 0.2, radius_of_the_search_ball );	
		if ( debug_mode )
		{
		  ROS_INFO ( "integrateField start = %f, %f, %f", start[0], start[1], start[2] ); 
		  //ROS_INFO("(rrtstar_field) field_path:");
		  //field_path.print(std::cout);
		}
                if ( field_path.getStateCount() >1 )
                {
                    calc_path.keepBefore ( calc_path.getState ( 0 ) ); // Remove all the states of calc_path but the start.
                    field_path.keepAfter ( field_path.getState ( 1 ) ); // Remove the start from the field path.
                    calc_path.append ( field_path );
                }
                else
                {
                    ROS_INFO ( "===========================" );
                    ROS_INFO ( " DESVIANDO DO OBSTACULO... " );
                    ROS_INFO ( "===========================" );
                }

            }
            
//             if ( debug_mode )
// 	      calc_path.print(std::cout);
            

            if ( debug_mode )
	      ROS_INFO( "rrtstar_field::main(). Path size = %d. Before SMOOTHNESS = %f", (int)calc_path.getStateCount(), calc_path.smoothness() );

	    unsigned int maxSteps=3;
	    path_simplifier.smoothBSpline(calc_path,maxSteps);
	    
            if ( debug_mode )
	      ROS_INFO( "rrtstar_field::main(). Path size = %d. After SMOOTHNESS = %f", (int)calc_path.getStateCount(), calc_path.smoothness() );

	    //calc_path.checkAndRepair(20);
	    calc_path.interpolate ( 100 ); // The final path will have 100 points
            
	    if ( debug_mode )
	      ROS_INFO( "rrtstar_field::main(). Path size = %d. After interpolate", (int)calc_path.getStateCount());
	    
//==================================		      src_point

        // Corta o path calculado a partir distancia de 'start_offset' metros.
	    
	int path_size = (int)calc_path.getStateCount();
	double minDist = 1000.0;
	
	if ( debug_mode )
	    ROS_INFO ( "PASSO 6.2" );
	
        if ( path_size > 0 )
        {
	  
	  ros::spinOnce();
	  drone_pose.pose = m100.getCurrPos();
	  
	drone_vel = m100.getVel();
	drone_vel_vec.x =  drone_vel.linear.x;
	drone_vel_vec.y =  drone_vel.linear.y;
	drone_vel_vec.z =  drone_vel.linear.z;
	drone_vel_mod = drone_vel_vec.module();
	
// 	  src_point.x = drone_pose.pose.position.x;
// 	  src_point.y = drone_pose.pose.position.y;
// 	  src_point.z = drone_pose.pose.position.z;
	  laserObstacle.setDronePose(drone_pose);	
	  
// 	  aux_state[0] = drone_pose.pose.position.x;
// 	  aux_state[1] = drone_pose.pose.position.y;
// 	  aux_state[2] = drone_pose.pose.position.z;
	  //final_path.append(aux_state->as<ob::State>());
	  
	   
	  if ( debug_mode )
	      ROS_INFO ( "(rrtstar_field) drone_pose = %f, %f, %f",
			  drone_pose.pose.position.x,
			  drone_pose.pose.position.y,
			  drone_pose.pose.position.z );	 
	  
            // Estima a distancia que o drone vai percorrer
           double estimated_dist = drone_vel_mod * solveTime;// + start_offset;
	   //double estimated_dist =  start_offset;
	    
	    int kinit = 0;
	    
	    if ( debug_mode )
		ROS_INFO ( "PASSO 6.3" );	    
  
            // Percorre os itens do path em ordem decrescente
	    bool startOk = false;
	    bool firstExec = true;
            for ( int k = path_size-1; k >= 0; k-- )
            {
                aux_state = calc_path.getState ( k );
// 		if ( debug_mode )
// 		{
// 		  ROS_INFO ( "PASSO 6.3.1" );
// 		  ROS_INFO ( "(rrtstar_field) k = %d, aux_state = %f, %f, %f",
// 			      k, aux_state[0], aux_state[1], aux_state[2] );		  
// 		  ROS_INFO ( "(rrtstar_field) drone_pose = %f, %f, %f",
// 			      drone_pose.pose.position.x,
// 			      drone_pose.pose.position.y,
// 			      drone_pose.pose.position.z );
// 		}
// 		
		
		

                // Mede a distancia do drone em relacao a estado k
                double dist = calcDist ( drone_pose.pose.position.x,
                                         drone_pose.pose.position.y,
                                         drone_pose.pose.position.z,
                                         aux_state[0], aux_state[1], aux_state[2] );
		
// 		// Armezena o valor k_nearst
// 		if( dist < minDist )
// 		{
// 		  minDist = dist;
// 		  k_nearst = k;
// 		}

/*		if ( debug_mode )
		  ROS_INFO ( "PASSO 6.3.2" );  	*/	

//// Define o dst_point baseado no path calculado
//                 if ( (dist > estimated_dist + dst_point_offset) || firstExec )
//                 {
// 		  firstExec = false;
// 		  
// 		  dst_point.x = aux_state[0];
// 		  dst_point.y = aux_state[1];
// 		  dst_point.z = aux_state[2];
// 		  laserObstacle.setDstPoint(dst_point);
// 		}


		// Encontra o valor de 'k' para a distancia for menor ou igual a estimada.
                if ( dist <= estimated_dist )
                {
		    
		    if(!startOk)
		    {
		      start = aux_state;
		      startOk = true;
		    }

		    if ( dist <= start_offset )
		    {
		      kinit = k;
		      
		      if ( debug_mode )
			  ROS_INFO ( "(rrtstar_field) k = %d, estimated_dist = %f, dist = %f, start_offset = %f",
				    k, estimated_dist, dist, start_offset );
		      
		      break;
		    }
                }
            }
            
	    if ( debug_mode )
		ROS_INFO ( "PASSO 6.4" );            
            // Se kinit for maior que zero, corta o path a partir do valor de 'k_init'
            if( kinit > 0 )
	    {
	      calc_path.keepAfter( calc_path.getState ( kinit ) );
	      if ( debug_mode )
		  ROS_INFO ( "(rrtstar_field) calc_path.keepAfter kinit = %d"
			      ", past_path_size = %d, path_size = %d"
			      ", pos_x = %f, pos_y = %f, pos_z = %f",
			      kinit, path_size, (int)calc_path.getStateCount(),
			      drone_pose.pose.position.x,
			      drone_pose.pose.position.y,
			      drone_pose.pose.position.z );	      
	    }
            
        }
//==================================	    
	    
	if ( debug_mode )
	    ROS_INFO ( "PASSO 6.5" );
    
	    optimizingPlanner->clear();
            pdef->clearSolutionPaths();

            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 7" );
            }

            //=============
            // PATH FOLLOW
            //-------------speed_pub
            drone_path.header.stamp=ros::Time::now();
            drone_path.header.frame_id="world";

            // ROS_INFO("\n\n\n");
            for ( int k=0; k< ( int ) calc_path.getStateCount(); k++ )
            {

                drone_pose.header.seq=k;
                aux_state = calc_path.getState ( k );
                drone_pose.pose.position.x = aux_state[0];
                drone_pose.pose.position.y = aux_state[1];
                drone_pose.pose.position.z = aux_state[2];
                drone_path.poses.push_back ( drone_pose );
                // ROS_INFO(" path %d: %f, %f", k, aux_state[0], aux_state[1]);
            }
            //ROS_INFO("\n\n\n");

            // ENVIA O CAMINHO QUE O DRONE DEVE PERCORRER
            path_pub.publish ( drone_path );
            drone_path.poses.clear();

            speed.data = droneVel;
            speed_pub.publish ( speed );

            //=============
            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 8" );
            }
            // ros::Duration(5.0).sleep();

            //path[i].keepBefore(path[i].getStatedrone_path(50));

            // Path calculado em vermelho
            if ( publishMarkers )
            {
                pub_path_marker.publish ( ompl_rviz::GetMarker ( calc_path, 0.1, 1, 0, 0, 1, dim ) );
            }

        }
        else
        {
            ROS_INFO_STREAM ( "No Solution Found" );

            /*ob::PlannerData data(si);
            optimizingPlanner->getPlannerData(data);
            pub_graph_marker.publish(ompl_rviz::GetGraph(data, dim, 0.05, 0, 0, 1, 0.3));
            */

            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 9" );
            }
            ros::Duration ( solveTime ).sleep();
        }

        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 10" );
        }

    } // while
  

  return 0;
  
}


int main ( int argc, char **argv )
{
    ROS_INFO("### (rrtstar_field) INIT ###");
	
    ros::init ( argc, argv, "rrtstar_field" );

    return rrtstar_field_main();
}






