#include <ros/ros.h>


using namespace std;

class Circle
{
public:
  double x, y, z, r;
  Circle( double x_=0, double y_=0, double z_=0, double r_=0 ): x(x_), y(y_), z(z_), r(r_) {}
  
  double distance( const Circle &c2 )
  {
    double dx = c2.x-x;
    double dy = c2.y-y;
    double dz = c2.z-z;
    return sqrt( dx*dx + dy*dy + dz*dz);
  }
  
  double intersection( const Circle &c2 )
  {
    double result = 0.0;
    double dist = distance(c2);
    double x1, y1, r1, x2, y2, r2;

    // Define o circulo 1 é menor que o 2
    if( r <  c2.r ) 
    {
      x1 = x;    y1 = y;    r1 = r;
      x2 = c2.x; y2 = c2.y; r2 = c2.r;
      //cout << "c1 menor" << endl;
    }
    else
    {
      x2 = x;    y2 = y;    r2 = r;
      x1 = c2.x; y1 = c2.y; r1 = c2.r;
      //cout << "c2 menor" << endl;
    }

    //cout << "dist = " << dist << endl;
    
    // Se um circulo está em cima dou outro a intersecao é de 100%
    if( dist < 0.0001 ) 
      return 1.0;
    
    if( dist < r1 + r2 ) // Existe interseção
    {
      // direção
      double nx = (x2-x1)/dist;
      double ny = (y2-y1)/dist;	
      
      // ponto do r1
      double px = x1 + nx*r1;
      double py = y1 + ny*r1;

      // ponto do r2
      double qx = x2 - nx*r2;
      double qy = y2 - ny*r2;
      
      double dx = qx-px;
      double dy = qy-py;
      
      result = sqrt( dx*dx + dy*dy ) / (2*r1);
      
//       cout << "r1 + r2 = " << r1 + r2 << endl;
//       cout << "nx, ny = " << nx << ", " << ny << endl;
//       cout << "px, py = " << px << ", " << py << endl;
//       cout << "qx, qy = " << qx << ", " << qy << endl;
//       cout << "dx, dy = " << dx << ", " << dy << endl;
//       cout << "result = " << result << endl;
      
      if( result > 1.0 )
	result = 1.0;
    }
    
    return result;
  }  

  bool collided( double x_, double y_, double z_ )
  {
    return( this->distance(Circle(x_,y_,z_)) <= r );
  }
};


int main(int argc, char** argv)
{
  ros::init(argc, argv, "test");
  ros::NodeHandle nh;  
  
  Circle c2(0,0,0,2), c1;
  
  ROS_INFO("Circulo 2 x = 0, r = 2");
  
  while(1)
  {
    ROS_INFO("Entre com o valor de x, y e raio do circulo 1: ");
    std::cin >> c1.x >> c1.y >> c1.r;
    if( c1.r == 0 )
      break;
      
    std::cout << "intersecao = " << c1.intersection(c2) << std::endl;
  }
  
}
