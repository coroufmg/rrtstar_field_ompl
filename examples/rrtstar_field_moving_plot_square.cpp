/* Copyright 2014 Guilherme pereira
 * rrtstar_field_moving_corridor.cpp
 *
 *  Created on: Dec 16, 2015 (Updated to remove ca_stacks on April 20, 2017)
 *      Author: Guilherme Pereira
 */

#include <ros/ros.h>
#include "rrtstar_field_navigation/rrtstar_field.h"
#include "ompl_rviz/ompl_rviz.h"


namespace ob = ompl::base;
namespace og = ompl::geometric;

using namespace ca::rrtstar_field;
using namespace coro;

class ValidityChecker : public ob::StateValidityChecker
{
public:
    ValidityChecker(const ob::SpaceInformationPtr& si) :
        ob::StateValidityChecker(si) {}
    // Returns whether the given state's position overlaps the
    // circular obstacle
    bool isValid(const ob::State* state) const
    {
      return true;
        //return this->clearance(state) > 0.0;
    }
    // Returns the distance from the given state's position to the
    // boundary of the circular obstacle.
    double clearance(const ob::State* state) const
    {
        // We know we're working with a RealVectorStateSpace in this
        // example, so we downcast state into the specific type.
        const ob::RealVectorStateSpace::StateType* state2D =
            state->as<ob::RealVectorStateSpace::StateType>();
        // Extract the robot's (x,y) position from its state
        double x = state2D->values[0];
        double y = state2D->values[1];
        // Distance formula between two points, offset by the circle's
        // radius
        return sqrt((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5)) - 10; // Circle of radius 10 centered at (0.5,0.5)
    }
};


// Redefinition of FollowVectorField cost function to follow a corridor
class FollowVectorFieldCorridor : public FollowVectorField
{
public: 
     FollowVectorFieldCorridor(const ob::SpaceInformationPtr &si) : FollowVectorField(si){}

     vec field(const ob::State *state) const
      {
  	double d0=5;
  	double a=0.1;
  	const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
  	vec v(1, a*(d0-s->values[1]));
  	v.normalize();
  	return v;  
       }
};




// Redefinition of FollowVectorField cost function to follow a square circulation
class FollowVectorFieldCirculation : public FollowVectorField
{
public: 
     FollowVectorFieldCirculation(const ob::SpaceInformationPtr &si) : FollowVectorField(si){}

     vec field(const ob::State *state) const
      {
	const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
        double x=s->values[0];
        double y=s->values[1];

	double x3=x*x*x;
        double y3=y*y*y; 
        double fi=x3*x+y3*y-1000.0;
        vec gradfi(4.0*x3, 4.0*y3);
	vec gradHfi(-gradfi.y, gradfi.x);
	double G = -0.636619772367581*atan(10*fi);
	double H = -sqrt(1.0-G*G);
        double h = 1.0/gradfi.module();	
	
        gradfi.normalize();  	
        gradfi.multiply(0.001*G);
	gradHfi.multiply(H*h);

	vec u=gradfi.sum(gradHfi);
	u.normalize();
	return u;  
       }
};


// This function is used to define the new objective which is to follow a corridor
ob::OptimizationObjectivePtr getFollowVectorFieldObjective(const ob::SpaceInformationPtr& si)
{
    return ob::OptimizationObjectivePtr(new FollowVectorFieldCirculation(si));
}


int main(int argc, char **argv) {

  ros::init(argc, argv, "ompl_rrtstar");
  ros::NodeHandle n("~");

  ros::Publisher pub_path_marker = n.advertise<visualization_msgs::Marker>("square_path", 1);
  ros::Publisher pub_graph_marker = n.advertise<visualization_msgs::Marker>("graph", 1);
  ros::Duration(1.0).sleep();

  // Parameter of the problem
  double radius_of_the_search_ball = 7.0;       // radius of the sphere
  double eta = 0.5; 			       // range of steering	 

  // Construct the robot state space in which we're planning. We're
  // planning in a subset of R^2.
  ob::StateSpacePtr space(new RealVectorStateSpaceField(2, radius_of_the_search_ball)); // the second parameter is the ball radius

  // Set the bounds of space   
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0, -30); // x
  bounds.setLow(1, -30); // y 
  bounds.setHigh(0, 30); // x
  bounds.setHigh(1, 30); // y
  space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

  // Construct a space information instance for this state space
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  // Set the object used to check which states in the space are valid 
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new ValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  // Create a problem instance
  ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));

  // Set the objective function
  pdef->setOptimizationObjective(getFollowVectorFieldObjective(si));

  // Set our robot's starting state 
  ob::ScopedState<> start(space), goal(space);
  start->as<ob::RealVectorStateSpace::StateType>()->values[0] = -5.8;
  start->as<ob::RealVectorStateSpace::StateType>()->values[1] = 0.0;
    
  goal->as<ob::RealVectorStateSpace::StateType>()->values[0] = 5.0;
  goal->as<ob::RealVectorStateSpace::StateType>()->values[1] = 5.0;
  // Set the start and goal states
  pdef->setStartAndGoalStates(start, goal);  // Start and Goal can be the same. This will not change the final tree

  space->as<RealVectorStateSpaceField>()->setStart(start); // This is necessary for the sampler.
    
  // Construct our optimizing planner using the RRTstar algorithm.
  RRTstarField *rrtstarPlanner = new RRTstarField(si);
  rrtstarPlanner->setRange(eta);
  rrtstarPlanner->setRewireFactor(1.01);
  rrtstarPlanner->setGoalBias(0);
  rrtstarPlanner->setBallRadius(radius_of_the_search_ball, 0.05*radius_of_the_search_ball);
  ob::PlannerPtr optimizingPlanner(rrtstarPlanner);
 
  // Set the problem instance for our planner to solve
  optimizingPlanner->setProblemDefinition(pdef);
 
  optimizingPlanner->setup();  
  
  int n_paths = 11; 
  std::vector<og::PathGeometric> path(n_paths, og::PathGeometric(si));
  og::PathGeometric final_path(si);
  FollowVectorFieldCirculation field(si); // This is necessary to compute the integral of the field
   
  for (int i=0; i<n_paths; i++){
	
	ROS_INFO("%f %f\n", start[0], start[1]);
  	ob::PlannerStatus solved = optimizingPlanner->solve(0.4);
  
  	if(solved) {
    
    		//ob::PlannerData data(si);
    		//optimizingPlanner->getPlannerData(data);   
		//pub_graph_marker.publish(ompl_rviz::GetGraph(data, 100, 0.05, 0, 0, 1, 0.3));
		
		if (rrtstarPlanner->computeFinalPath())	     // This computes the path. If you do not run this, you have standard RRT* path
		  ROS_INFO("Found path!");
		
		if (rrtstarPlanner->foundPath())	     // This indicates if it found a path. This is important if the borders of the searching radius are blocked.	
		  ROS_INFO("Found path!");
		
		// pdef->getSolutionPath()->print(std::cout);
		path[i]=dynamic_cast<const og::PathGeometric& >( *pdef->getSolutionPath());
		
		// Integrate the field. If the integral of the field do not collide, use it as a path.
		og::PathGeometric field_path=field.integrateField(start->as<ob::State>(), 0.5, radius_of_the_search_ball);
		if (field_path.getStateCount()>1){
		     path[i].keepBefore(path[i].getState(0)); // Remove all the states of path[i] but the start.
		     field_path.keepAfter(field_path.getState(1)); // Remove the start from the field path.
		     path[i].append(field_path);
		}
		
		path[i].interpolate(100); // The final path will have 100 points
    		
		optimizingPlanner->clear();
		pdef->clearSolutionPaths();
		
		// set new start position
		start=path[i].getState(50); // Aproximatelly 50% of the path
		    
  		// Set the start and goal states
  		pdef->setStartAndGoalStates(start, goal);  // Start and Goal can be the same. This will not change the final tree

  		space->as<RealVectorStateSpaceField>()->setStart(start); // This is necessary for the sampler.
 				
  		path[i].keepBefore(path[i].getState(50));
		pub_path_marker.publish(ompl_rviz::GetMarker(path[i], 0.1, 0, 1, 0, 1));
		ROS_INFO("After prunning %d\n", (int)path[i].getStateCount());
		
		ROS_INFO("Final Path Size %d\n", (int)final_path.getStateCount());
		if (final_path.getStateCount()>0)
			final_path.append((og::PathGeometric)path[i]);
		else
			final_path=path[i];
		
  	}
  	else  {
    		ROS_INFO_STREAM("No Solution Found");

    		//ob::PlannerData data(si);
    		//optimizingPlanner->getPlannerData(data);
    		//pub_graph_marker.publish(ompl_rviz::GetGraph(data, 100, 0.05, 0, 0, 1, 0.3));
		
		break;
  	}

  	if (!ros::ok()) break;
   } // for	

   ros::Rate loop_rate(1.0);
   while(ros::ok())
   {
      ros::spinOnce();
      ROS_INFO("(pub_path_marker)!");
      pub_path_marker.publish(ompl_rviz::GetMarker(final_path, 0.05, 0, 0, 0, 1));
      loop_rate.sleep();     
   }
}






