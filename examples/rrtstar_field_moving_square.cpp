/* Copyright 2014 Guilherme pereira
 * rrtstar_field_moving_square.cpp
 *
 *  Created on: Dec 16, 2015 (Updated to remove ca_stacks on April 20, 2017)
 *      Author: Guilherme Pereira
 * Modified by: Henrique Nunes Machado (January, 2018)
 */

//#define HECTOR_SIMU

#include <ros/ros.h>
//#include <tf/transform_listener.h>

#include "rrtstar_field_navigation/rrtstar_field.h"
#include "rrtstar_field_navigation/scan_ob.h"
#include "ompl_rviz/ompl_rviz.h"

#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <ompl/geometric/PathSimplifier.h>

//#include <sensor_msgs/LaserScan.h>
//#include <sensor_msgs/PointCloud.h>
//#include <laser_geometry/laser_geometry.h>

// Simulação do DJI M100 a aprtir do simulador Hector Quadrotor

#ifdef HECTOR_SIMU
#include <dji_m100/m100_hector.h>
#else
#include <dji_m100/m100.h>
#endif

namespace ob = ompl::base;
namespace og = ompl::geometric;

using namespace ca::rrtstar_field;
using namespace coro;



/*ros::Publisher pub_cloud;
laser_geometry::LaserProjection projector_laser;

void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in)
{
  sensor_msgs::PointCloud cloud;
  projector_laser.projectLaser(*scan_in, cloud);

  // Do something with cloud.
  //pub_cloud.publish<>();
  pub_cloud.publish(cloud);

}*/
/*
sensor_msgs::LaserScan scan_pts;
void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in)
{
  scan_pts = *scan_in;
  //projector_laser.projectLaser(*scan_in, cloud);

  // Do something with cloud.
  //pub_cloud.publish<>();
  //pub_cloud.publish(cloud);

}*/

/*geometry_msgs::Point toEulerAngle(geometry_msgs::Quaternion quat)
{
  geometry_msgs::Point ans;

  tf::Matrix3x3 R_FLU2ENU(tf::Quaternion(quat.x, quat.y, quat.z, quat.w));
  R_FLU2ENU.getRPY(ans.x, ans.y, ans.z);
  return ans;
}*/


// Callback para esperar o um sinal de start do nó 'path_follow_mav' que
// executa após o drone ter decolado e estar pronto para executar o algoritmo.
std_msgs::Bool wasStarted;
void WasStarted ( const std_msgs::Bool::ConstPtr &msg )
{
    wasStarted = *msg;
}

// Define um obstáculo
class ValidityChecker : public ob::StateValidityChecker
{
public:
    ValidityChecker ( const ob::SpaceInformationPtr &si ) :
        ob::StateValidityChecker ( si ) {}
    // Returns whether the given state's position overlaps the
    // circular obstacle
    bool isValid ( const ob::State *state ) const
    {
        return this->clearance ( state ) > 0.0;
    }
    // Returns the distance from the given state's position to the
    // boundary of the circular obstacle.
    double clearance ( const ob::State *state ) const
    {
        // We know we're working with a RealVectorStateSpace in this
        // example, so we downcast state into the specific type.
        const ob::RealVectorStateSpace::StateType *state2D =
            state->as<ob::RealVectorStateSpace::StateType>();
        // Extract the robot's (x,y) position from its state
        double x = state2D->values[0];
        double y = state2D->values[1];
        // Distance formula between two points, offset by the circle's
        // radius
        //return sqrt((x-5.5)*(x-5.5) + (y-0.1)*(y-0.1)) - 2.5; // Circle of radius 10 centered at (0.5,0.5)
        return sqrt ( ( x-5.5 ) * ( x-5.5 ) + ( y-0.1 ) * ( y-0.1 ) ) - 3.5; // Circle of radius 10 centered at (0.5,0.5)
    }
};


// Redefinition of FollowVectorField cost function to follow a corridor
class FollowVectorFieldCorridor : public FollowVectorField
{
public:
    FollowVectorFieldCorridor ( const ob::SpaceInformationPtr &si ) : FollowVectorField ( si ) {}

    vec field ( const ob::State *state ) const
    {
        double d0=5;
        double a=0.1;
        const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
        vec v ( 1, a* ( d0-s->values[1] ) );
        v.normalize();
        return v;
    }
};


// Redefinition of FollowVectorField cost function to follow a square circulation
class FollowVectorFieldCirculation : public FollowVectorField
{
public:
    FollowVectorFieldCirculation ( const ob::SpaceInformationPtr &si ) : FollowVectorField ( si ) {}

    vec field ( const ob::State *state ) const
    {
        const ob::RealVectorStateSpace::StateType *s = state->as<ob::RealVectorStateSpace::StateType>();
        double x=s->values[0];
        double y=s->values[1];

        double x3=x*x*x;
        double y3=y*y*y;
        double fi=x3*x+y3*y-1000.0;
        vec gradfi ( 4.0*x3, 4.0*y3 );
        vec gradHfi ( -gradfi.y, gradfi.x );
        //double G = -0.636619772367581*atan(10*fi); // atan(x*fi) - quanto maior o valor de x, converge mais rápido.
        double G = -0.636619772367581*atan ( 1000*fi );
        double H = -sqrt ( 1.0-G*G );
        double h = 1.0/gradfi.module();

        /*
        	double x3=x*x*x;
                double y3=y*y*y;
                double fi=x3*x+y3*y-160000.0;
                vec gradfi(4.0*x3, 4.0*y3);
        	vec gradHfi(-gradfi.y, gradfi.x);
        	double G = -0.636619772367581*atan(10*fi);
        	double H = -sqrt(1.0-G*G);
                double h = 1.0/gradfi.module();*/


        /*double fi=pow(x,4)+pow(y,4)-160000;
              vec gradfi(4*pow(x,3), 4*pow(y,3));
        vec gradHfi(-gradfi.y, gradfi.x);
        double G = -(2.0/M_PI)*atan(10*fi);
        double H = -sqrt(1.0-pow(G,2));
              double h = 1.0/gradfi.module();*/

        gradfi.normalize();
        gradfi.multiply ( 0.001*G );
        gradHfi.multiply ( H*h );

        vec u=gradfi.sum ( gradHfi );
        u.normalize();
        return u;
    }
};


// This function is used to define the new objective which is to follow a corridor
ob::OptimizationObjectivePtr getFollowVectorFieldObjective ( const ob::SpaceInformationPtr &si )
{
    return ob::OptimizationObjectivePtr ( new FollowVectorFieldCirculation ( si ) );
}


double calcDist ( double x1, double y1, double x2, double y2 )
{
    double dx = x2-x1;
    double dy = y2-y1;
    return sqrt ( dx*dx + dy*dy );
}



int main ( int argc, char **argv )
{
    const int dim = 2;
    
    visualization_msgs::Marker m;
    m.header.frame_id = "base_link";
    m.ns = "drone";
    //m.id = 0;
    m.action = visualization_msgs::Marker::ADD;
    m.type = visualization_msgs::Marker::ARROW; //correct marker type
    m.color.r = 0.0; m.color.g = 0.0; m.color.b = 1.0; m.color.a = 1.0;    


    ROS_INFO("rrtstar_field init");

//  ros::init(argc, argv, "ompl_rrtstar");
//  ros::NodeHandle n("~");
    ros::init ( argc, argv, "rrtstar_field" );
    ros::NodeHandle n;

    // Parameter of the problem
    double radius_of_the_search_ball, eta, squareArea, resolution, solveTime, droneVel, start_offset;
    double minObjDist, maxObjDist, droneRadius;
    bool publishMarkers, integrateField, debug_mode, laser_enable;

    //double radius_of_the_search_ball = 10;       // radius of the sphere
    //double eta = 2; 			       // range of steering
    //double squareArea = 10;
    //droneVel = 1.0
    //solveTime = 1.0
    // resolution = 0.2

    n.param ( "/rrtstar_field/radius_of_the_search_ball", radius_of_the_search_ball, 5.0 );
    n.param ( "/rrtstar_field/eta", eta, 1.0 );
    n.param ( "/rrtstar_field/squareArea", squareArea, 15.0 );
    n.param ( "/rrtstar_field/resolution", resolution, 0.2 );

    n.param("/rrtstar_field/minObjDist", minObjDist, 1.0);
    n.param("/rrtstar_field/maxObjDist", maxObjDist, 7.5);
    n.param("/rrtstar_field/droneRadius", droneRadius, 2.0);

    n.param("/rrtstar_field/solveTime", solveTime, 0.6);
    n.param("/rrtstar_field/droneVel", droneVel, 1.0);
    n.param("/rrtstar_field/start_offset", start_offset, 0.0);
    n.param("/rrtstar_field/publishMarkers", publishMarkers, true);
    n.param("/rrtstar_field/integrateField", integrateField, true);
    n.param ( "/rrtstar_field/debug_mode", debug_mode, true );
    n.param ( "/rrtstar_field/laser_enable", laser_enable, true );


    //ROS_INFO("params: radius_of_the_search_ball = %f, eta = %f, squareArea = %f, resolution = %f, solveTime = %f, droneVel = %f",
//	   radius_of_the_search_ball, eta, squareArea, resolution, solveTime, droneVel);


    // Reduz o tamanho do path por este fator
    // Path reduction factor
    //double factor = 1.0;
    ROS_INFO ( "(rrtstar_field) INICIO." );

    //=============
    // PATH FOLLOW
    //-------------
    // Subscriber para esperar o um sinal de start do nó 'path_follow_mav'
    //ros::Subscriber scan_sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 1, scanCallback);

    // advertise
    //pub_cloud = n.advertise<sensor_msgs::PointCloud>("/scan_cloud", 1);



    ros::Subscriber start_sub = n.subscribe<std_msgs::Bool> ( "/path_follow/start", 1, WasStarted );

    // Path publisher
    ros::Publisher path_pub = n.advertise<nav_msgs::Path> ( "/path_follow/path", 1 );
    // Speed publisher
    ros::Publisher speed_pub = n.advertise<std_msgs::Float32> ( "/path_follow/speed", 1 );

    nav_msgs::Path drone_path;
    std_msgs::Float32 speed;
    geometry_msgs::PoseStamped drone_pose;
    int num_points=100;
    //=============

    tf::TransformListener listener;

    ros::Publisher pub_path_marker = n.advertise<visualization_msgs::Marker> ( "/rrtstar_field/path", 1 );
    ros::Publisher pub_graph_marker = n.advertise<visualization_msgs::Marker> ( "/rrtstar_field/graph", 1 );
    ros::Publisher pub_path_limit_cicle = n.advertise<visualization_msgs::Marker> ( "/rrtstar_field/limit_cicle", 1 );

    ros::Publisher pub_vel_marker = n.advertise<visualization_msgs::Marker> ( "/rrtstar_field/vel", 1 );
    
    
    ros::Duration ( 1.0 ).sleep();


    // Construct the robot state space in which we're planning. We're
    // planning in a subset of R^2.
    ob::StateSpacePtr space ( new RealVectorStateSpaceField ( 2, radius_of_the_search_ball ) ); // the second parameter is the ball radius

    // Set the bounds of space
    ob::RealVectorBounds bounds ( 2 );
    bounds.setLow ( 0, -squareArea ); // x
    bounds.setLow ( 1, -squareArea ); // y
    bounds.setHigh ( 0, squareArea ); // x
    bounds.setHigh ( 1, squareArea ); // y
    space->as<ob::RealVectorStateSpace>()->setBounds ( bounds );

    // Construct a space information instance for this state space
    ob::SpaceInformationPtr si ( new ob::SpaceInformation ( space ) );


    // Declara obstaculo do tipo laser
    scan_ob laserObstacle ( si,n,"/scan",debug_mode );
    si->setStateValidityChecker ( ob::StateValidityCheckerPtr ( &laserObstacle ) );

    // A máxima distancia em que o laser considera um obstaculo é 50% maior que o raio do rrt ball.
    //laserObstacle.setMaxDist( radius_of_the_search_ball * 1.5 );
    /*laserObstacle.setMinDist( minObjDist );
    laserObstacle.setMaxDist( maxObjDist );
    laserObstacle.setStateRadius( droneRadius );
    */
    // Set the object used to check which states in the space are valid
    //si->setStateValidityChecker(ob::StateValidityCheckerPtr(new ValidityChecker(si)));


    si->setStateValidityCheckingResolution ( resolution );
    si->setup();

    // Create a problem instance
    ob::ProblemDefinitionPtr pdef ( new ob::ProblemDefinition ( si ) );
//
    // Set the objective function
    pdef->setOptimizationObjective ( getFollowVectorFieldObjective ( si ) );

    // Set our robot's starting state
    ob::ScopedState<> start ( space ), goal ( space ), aux_state ( space ), bestNearStart ( space ), lastValidState ( space );
    start->as<ob::RealVectorStateSpace::StateType>()->values[0] = -5.0;
    start->as<ob::RealVectorStateSpace::StateType>()->values[1] = -5.0;

    goal->as<ob::RealVectorStateSpace::StateType>()->values[0] = 10.0;
    goal->as<ob::RealVectorStateSpace::StateType>()->values[1] = 10.0;


    //laserObstacle.isValid(start->as<ob::State>());
    //laserObstacle.clearance(goal->as<ob::State>());


    // Set the start and goal states
    pdef->setStartAndGoalStates ( start, goal ); // Start and Goal can be the same. This will not change the final tree

    space->as<RealVectorStateSpaceField>()->setStart ( start ); // This is necessary for the sampler.

    // Construct our optimizing planner using the RRTstar algorithm.
    RRTstarField *rrtstarPlanner = new RRTstarField ( si );
    rrtstarPlanner->setRange ( eta );
    rrtstarPlanner->setRewireFactor ( 1.01 );
    rrtstarPlanner->setGoalBias ( 0 );
    rrtstarPlanner->setBallRadius ( radius_of_the_search_ball, 0.05*radius_of_the_search_ball );
    ob::PlannerPtr optimizingPlanner ( rrtstarPlanner );

    // Set the problem instance for our planner to solve
    optimizingPlanner->setProblemDefinition ( pdef );

    optimizingPlanner->setup();

    //std::vector<og::PathGeometric> path(n_paths, og::PathGeometric(si));
    og::PathGeometric final_path ( si );
    og::PathGeometric calc_path ( si );
    FollowVectorFieldCirculation field ( si ); // This is necessary to compute the integral of the field
    
    og::PathSimplifier path_simplifier(si);

    //=======
    // DRONE
    //-------

#ifdef HECTOR_SIMU
    coro::M100_hector m100 ( n,true ); // Read only
#else
    coro::M100 m100 ( n,true ); // Read only
#endif

    ros::Rate start_rate ( 2 );
    ROS_INFO ( "(rrtstar_field) Waiting for isAutonomous signal!" );
    while ( ros::ok() )
    {
        ros::spinOnce();

        if ( m100.isAutonomous() )
        {
            if ( m100.initZeroPos() )
            {
                ROS_INFO ( "(rrtstar_field) initZeroPos!" );
                break;
            }
        }

        start_rate.sleep();
    }

    //=======


    ros::Rate loop_rate ( 0.5 );
    while ( !wasStarted.data && ros::ok() ) // Wait for the path
    {
        ros::spinOnce();
        ROS_INFO ( "(rrtstar_field) Waiting for start signal!" );
        loop_rate.sleep();
    }



    if ( debug_mode )
    {
        ROS_INFO ( "PASSO 1" );
    }
    // ros::Duration(5.0).sleep();

    //ros::Time t_begin = ros::Time::now();
    //ros::Time t_end = t_begin + ros::Duration(solveTime);
    //ros::Duration t_loop = ros::Duration(solveTime);

    // Faz a frequencia metado do solve time.
    double T_loop = solveTime*2.0;
     ros::Rate main_loop_rate ( 1.0/T_loop );

    //for (int i=0; i<n_paths; i++){
    //bool firstLoop = true;
    double mav_heigth;
    while ( ros::ok() )
    {
	// Leitura dos parâmetros variáveis
        n.param ( "/rrtstar_field/minObjDist", minObjDist, 1.0 );
        n.param ( "/rrtstar_field/maxObjDist", maxObjDist, 7.5 );
        n.param ( "/rrtstar_field/droneRadius", droneRadius, 2.0 );

        n.param ( "/rrtstar_field/solveTime", solveTime, 0.6 );
        n.param ( "/rrtstar_field/droneVel", droneVel, 1.0 );
        n.param ( "/rrtstar_field/start_offset", start_offset, 0.0 );
        n.param ( "/rrtstar_field/publishMarkers", publishMarkers, true );
        n.param ( "/rrtstar_field/integrateField", integrateField, true );
	
	n.param ( "/rrtstar_field/mav_heigth", mav_heigth, 3.0 );   	

	if(laser_enable) 
	{
	  laserObstacle.setMinDist ( minObjDist );
	  laserObstacle.setMaxDist ( maxObjDist );
	  laserObstacle.setStateRadius ( droneRadius );
	}

        //=========================
        // set new start position
        ros::spinOnce();

        // Testa uma parada de emergencia se o laser não estiver funcionando adequadamente
	if(laser_enable) 
	{
	  if ( !laserObstacle.isScanOk() )
	  {
	      n.setParam ( "/emergency_stop", true );
	      ROS_ERROR ( "(rrtstar_field) EMERGENCY STOP!" );
	      ros::Duration ( solveTime ).sleep();
	      continue;
	  }
        }
        
	main_loop_rate.sleep(); // Sleep para não sobrecarregar processamento.

        // Atualiza a lista de onstaculos detectados pelo laser.
        // Faz a leitura dos obstáculos pŕoximos ao drone
        if(laser_enable) 
	  laserObstacle.getObstacleCircles( listener );
        ros::Duration ( 0.01 ).sleep();
        drone_pose.pose = m100.getCurrPos();


        ros::spinOnce();

        // START POSITION
        //start[0] = drone_pose.pose.position.x*factor;
        //start[1] = drone_pose.pose.position.y*factor;
        start[0] = drone_pose.pose.position.x;
        start[1] = drone_pose.pose.position.y;
	laserObstacle.setDronePose(drone_pose);		
	
	
	//double droneAngle = toEulerAngle(drone_pose.pose.orientation).z;

        if ( debug_mode )
        {
            ROS_INFO ( "(rrtstar_field) drone position = %f, %f", start[0], start[1] );
        }

//         // Faz a previsão de onde o drone vai estar após o solve do RRT* e define o start position.
//         if ( calc_path.getStateCount() > 0 )
//         {
// 	  
// 	  
// 	 
//             // Estima a distancia que o drone vai percorrer
//             double estimated_dist = droneVel * solveTime + start_offset;
// 	    
// 	    int kinit = 0;
//  /* 
//             // Percorre os itens do path em ordem decrescente
//             for ( int k = ( int ) calc_path.getStateCount()-1; k >= 0; k-- )
//             {
//                 aux_state = calc_path.getState ( k );
// 
//                 // Mede a distancia do drone em relacao a estado k
//                 double dist = calcDist ( drone_pose.pose.position.x,
//                                          drone_pose.pose.position.y,
//                                          aux_state[0], aux_state[1] );
// 
//                 // Se a distancia for menor ou igual ao estimado, define como ponto de start do RRT.
//                 if ( dist <= estimated_dist )
//                 {
//                     start = aux_state;
//                     if ( debug_mode )
//                         ROS_INFO ( "(rrtstar_field) k = %d, estimated_dist = %f, dist = %f, start_offset = %f",
//                                    k, estimated_dist, dist, start_offset );
// 			
// 		    kinit = k;
//                     break;
//                 }
//             }
//             */
//             
//             
// 	/*drone_pose.pose.position.x = start[0];
// 	drone_pose.pose.position.y = start[1];
// 	laserObstacle.setDronePose(drone_pose);	  */          
//             
//             // Percorre os itens do path em ordem crescente até achar um estado válido
//             for ( int k = kinit; k < (int)calc_path.getStateCount(); k++ )
//             {
// 	      aux_state = calc_path.getState( k );
// 	      bool isValidState = si->isValid ( aux_state->as<ob::State>() );
// 	      if(isValidState)
// 	      {
// 		start = aux_state;
// 		
// 		if ( debug_mode )
// 		    ROS_INFO ( "(rrtstar_field) NEW k = %d",k);		
// 		break;
// 	      }
// 	    }
//             
//         }

//         if ( debug_mode )
//         {
//             ROS_INFO ( "(rrtstar_field) start = %f, %f", start[0], start[1] );
//         }

        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 2" );
        }
        
	/*drone_pose.pose.position.x = start[0];
	drone_pose.pose.position.y = start[1];
	laserObstacle.setDronePose(drone_pose);	   */     

        ////////////////////////////////////////////////////////////////
        // Se a posição estimado do start estiver no obstáculo, calcula 
        // uma posição mais próxima do start fora do obstáculo.

        // Test if is a valid start position (out of obstacle).
        bool isValidState = si->isValid ( start->as<ob::State>() );
        if ( !isValidState )
        {
            ROS_INFO ( "===================" );
            ROS_INFO ( "POSICAO INVALIDA!!!" );
            ROS_INFO ( "===================" );

            ROS_INFO ( " start invalido = %f, %f", start[0], start[1] );
	    
	    if(laser_enable) 
	      laserObstacle.getBestValidState ( start->as<ob::State>() );
            ROS_INFO ( " START VALIDO CALCULADO = %f, %f", start[0], start[1] );

            if ( !si->isValid ( start->as<ob::State>() ) )
            {
                ROS_INFO ( "bestNearStart is not a valid state." );
                start = lastValidState;
            }
        }
        else
        {
            lastValidState = start;
        }

        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 3" );
        }

        // Set the start and goal states.
        // Start and Goal can be the same. This will not change the final tree
        pdef->setStartAndGoalStates ( start, goal ); 

	 // This is necessary for the sampler.
        space->as<RealVectorStateSpaceField>()->setStart ( start );
	
	drone_pose.pose.position.x = start[0];
	drone_pose.pose.position.y = start[1];
	laserObstacle.setDronePose(drone_pose);	

        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 4" );
        }

        // Calculate rrt solution during 'x' seconds
        // Maior tempo de processamento do loop.
        ob::PlannerStatus solved = optimizingPlanner->solve ( solveTime );
	ros::spinOnce();
        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 5" );
        }

        // Publica os a visualisão dos obstáculos só após calcular o RRT
        if(laser_enable) 
	  laserObstacle.pubVisCircles(T_loop);
        
        if ( solved )
        {
	    // Publica dados da árvore do RRT apenas para visualisão.
            if ( publishMarkers )
            {
                ob::PlannerData data ( si );
                optimizingPlanner->getPlannerData ( data );
                pub_graph_marker.publish ( ompl_rviz::GetGraph ( data, dim, 0.02, 0, 0, 1, 0.3 ) );
            }

            // Mostra o ciclo limite do campo
             if(publishMarkers)
             {
		//og::PathGeometric field_complete_path=field.integrateField(start->as<ob::State>(), 0.5, 50.0);
		//pub_path_limit_cicle.publish(ompl_rviz::GetMarker(field_complete_path, 0.1, 0, 1, 1, 1, dim));
             }

	    // This computes the path. If you do not run this, you have standard RRT* path
            if ( rrtstarPlanner->computeFinalPath() )
                if ( debug_mode )
                {
                    ROS_INFO ( "Found path!" );
                }

            // This indicates if it found a path. This is important if the borders of the
            // searching radius are blocked.
            if ( rrtstarPlanner->foundPath() )
                if ( debug_mode )
                {
                    ROS_INFO ( "Found path!" );
                }

            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 6" );
            }

            // pdef->getSolutionPath()->print(std::cout);
            calc_path=dynamic_cast<const og::PathGeometric & > ( *pdef->getSolutionPath() );
	    
            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 6.1" );
            }

            // Integrate the field. If the integral of the field do not collide, use it as a path.
            if ( integrateField )
            {
                og::PathGeometric field_path=field.integrateField ( start->as<ob::State>(), 0.2, radius_of_the_search_ball );
                if ( field_path.getStateCount() >1 )
                {
                    calc_path.keepBefore ( calc_path.getState ( 0 ) ); // Remove all the states of calc_path but the start.
                    field_path.keepAfter ( field_path.getState ( 1 ) ); // Remove the start from the field path.
                    calc_path.append ( field_path );
                }
                else
                {
                    ROS_INFO ( "===========================" );
                    ROS_INFO ( " DESVIANDO DO OBSTACULO... " );
                    ROS_INFO ( "===========================" );
                }

            }

            if ( debug_mode )
	      ROS_INFO( "rrtstar_field::main(). Path size = %d. Before SMOOTHNESS = %f", (int)calc_path.getStateCount(), calc_path.smoothness() );

	    //unsigned int maxSteps=3;
	    //path_simplifier.smoothBSpline(calc_path,maxSteps);
	    
            //if ( debug_mode )
	    //  ROS_INFO( "rrtstar_field::main(). Path size = %d. After SMOOTHNESS = %f", (int)calc_path.getStateCount(), calc_path.smoothness() );

	    //calc_path.checkAndRepair(20);
	    calc_path.interpolate ( 100 ); // The final path will have 100 points
            
	    if ( debug_mode )
	      ROS_INFO( "rrtstar_field::main(). Path size = %d. After interpolate", (int)calc_path.getStateCount());
	    
	    
	    
	    
	    
	    
//==================================
        // Corta o path calculado a partir distancia de 'start_offset' metros.
	    
	int path_size = (int)calc_path.getStateCount();
	
        if ( path_size > 0 )
        {
	  
	  ros::spinOnce();
	   drone_pose.pose = m100.getCurrPos();
	  
            // Estima a distancia que o drone vai percorrer
            //double estimated_dist = droneVel * solveTime + start_offset;
	   double estimated_dist =  start_offset;
	    
	    int kinit = 0;
  
            // Percorre os itens do path em ordem decrescente
            for ( int k = path_size-1; k >= 0; k-- )
            {
                aux_state = calc_path.getState ( k );

                // Mede a distancia do drone em relacao a estado k
                double dist = calcDist ( drone_pose.pose.position.x,
                                         drone_pose.pose.position.y,
                                         aux_state[0], aux_state[1] );

                // Encontra o valor de 'k' para a distancia for menor ou igual a estimada.
                if ( dist <= estimated_dist )
                {
                    start = aux_state;
                    if ( debug_mode )
                        ROS_INFO ( "(rrtstar_field) k = %d, estimated_dist = %f, dist = %f, start_offset = %f",
                                   k, estimated_dist, dist, start_offset );
			
		    kinit = k;
                    break;
                }
            }
            
            // Se kinit for maior que zero, corta o path a partir do valor de 'k_init'
            if( kinit > 0 )
	    {
	      calc_path.keepAfter( calc_path.getState ( kinit ) );
	      if ( debug_mode )
		  ROS_INFO ( "(rrtstar_field) calc_path.keepAfter kinit = %d"
			      ", past_path_size = %d, path_size = %d"
			      ", pos_x = %f, pos_y = %f",
			      kinit, path_size, (int)calc_path.getStateCount(),
			      drone_pose.pose.position.x,
			      drone_pose.pose.position.y );	      
	    }
            
        }
//==================================	    
	    
	    
	    
		    
	    
	    
	    
	    

	    optimizingPlanner->clear();
            pdef->clearSolutionPaths();

            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 7" );
            }

            //=============
            // PATH FOLLOW
            //-------------speed_pub
            drone_path.header.stamp=ros::Time::now();
            drone_path.header.frame_id="world";

            // ROS_INFO("\n\n\n");
            for ( int k=0; k< ( int ) calc_path.getStateCount(); k++ )
            {

                drone_pose.header.seq=k;
                aux_state = calc_path.getState ( k );
                drone_pose.pose.position.z = mav_heigth;
                drone_pose.pose.position.x = aux_state[0];
                drone_pose.pose.position.y = aux_state[1];
                drone_path.poses.push_back ( drone_pose );
                // ROS_INFO(" path %d: %f, %f", k, aux_state[0], aux_state[1]);
            }
            //ROS_INFO("\n\n\n");

            // ENVIA O CAMINHO QUE O DRONE DEVE PERCORRER
            path_pub.publish ( drone_path );
            drone_path.poses.clear();

            speed.data = droneVel;
            speed_pub.publish ( speed );

            //=============
            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 8" );
            }
            // ros::Duration(5.0).sleep();

            //path[i].keepBefore(path[i].getState(50));

            // Teste mHenrique
            if ( publishMarkers )
            {
                pub_path_marker.publish ( ompl_rviz::GetMarker ( calc_path, 0.1, 1, 0, 0, 1, dim ) );
            }

        }
        else
        {
            ROS_INFO_STREAM ( "No Solution Found" );

            /*ob::PlannerData data(si);
            optimizingPlanner->getPlannerData(data);
            pub_graph_marker.publish(ompl_rviz::GetGraph(data, dim, 0.05, 0, 0, 1, 0.3));
            */

            if ( debug_mode )
            {
                ROS_INFO ( "PASSO 9" );
            }
            ros::Duration ( solveTime ).sleep();
        }

        if ( debug_mode )
        {
            ROS_INFO ( "PASSO 10" );
        }

    } // while

}










