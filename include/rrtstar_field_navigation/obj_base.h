#ifndef OBJ_BASE_H
#define OBJ_BASE_H

#include <ros/ros.h>

namespace coro {

  // Class to encapsulate a vector in 2D
  class vec
  {
    public:
	  double x, y; // Components of a vector
	  
	  vec() : x(0.0), y(0.0) {}

	  vec(double a, double b) : x(a), y(b)
		  {}
	  
	  void normalize(){
		  double module = sqrt(x*x+y*y);
		  x/=module;
		  y/=module;
	  }

	  double innerProduct(vec a){
		  return x*a.x+y*a.y;
		  
	  }

	  double module(){
		  return sqrt(x*x+y*y);
	  }

	  vec sum(vec a){
		  vec b(x+a.x, y+a.y);
		  return b;

	  }

	  void multiply(double a){
		    x*=a;
		    y*=a;
	  }

  }; // class vec

  // Class to encapsulate a vector in 3D
  class vec3
  {
    public:
	  double x, y, z; // Components of a vector
	  
	  vec3() : x(0.0), y(0.0), z(0.0) {}
	  
	  vec3(double a, double b, double c) : x(a), y(b), z(c)
		  {}
	  
	  void normalize(){
		  double module = sqrt(x*x+y*y+z*z);
		  x/=module;
		  y/=module;
		  z/=module;
	  }

	  double innerProduct(vec3 a) const{
		  return x*a.x+y*a.y+z*a.z;	
	  }

	  double module() const{
		  return sqrt(x*x+y*y+z*z);
	  }

	  vec3 sum(vec3 a) const{
		  vec3 b(x+a.x, y+a.y, z+a.z);  
		  return b;
	  }
	  
	  vec3 dif(vec3 a) const{
		  vec3 b(x-a.x, y-a.y, z-a.z);  
		  return b;
	  }  		

	  vec3 cross(vec3 a) const{
		  vec3 b(y * a.z - z * a.y, z * a.x - x * a.z, x * a.y - y * a.x);  
		  return b;
	  }

	  void multiply(double a){
		    x*=a;
		    y*=a;
		    z*=a;
	  }

  }; // class vec3
  

  class Circle
  {
  public:
    double x, y, z, r;
    ros::Time time_stamp;
    
    Circle( double x_=0, double y_=0, double z_=0, double r_=0 ): x(x_), y(y_), z(z_), r(r_) {}
    
    double distance( const Circle &c2 ) const
    {
      double dx = c2.x-x;
      double dy = c2.y-y;
      double dz = c2.z-z;
      return sqrt( dx*dx + dy*dy + dz*dz);
    }
    
    double intersection( const Circle &c2 ) const
    {
      double result = 0.0;
      double dist = distance(c2);
      double x1, y1, z1, r1, x2, y2, z2, r2;

      // Define o circulo 1 menor que o 2
      if( r <  c2.r ) 
      {
	x1 = x;    y1 = y;    r1 = r;    z1 = z;
	x2 = c2.x; y2 = c2.y; r2 = c2.r; z2 = c2.z;
	//cout << "c1 menor" << endl;
      }
      else
      {
	x2 = x;    y2 = y;    r2 = r;    z2 = z;
	x1 = c2.x; y1 = c2.y; r1 = c2.r; z1 = c2.z;
	//cout << "c2 menor" << endl;
      }
      //cout << "dist = " << dist << endl;
      
      // Se um circulo está em cima do outro a intersecao é de 100%
      if( dist < 0.001 ) 
	return 1.0;
      
      if( dist < r1 + r2 ) // Existe interseção
      {
	// direção
	double nx = (x2-x1)/dist;
	double ny = (y2-y1)/dist;	
	double nz = (z2-z1)/dist;	
	
	// ponto do r1
	double px = x1 + nx*r1;
	double py = y1 + ny*r1;
	double pz = z1 + nz*r1;

	// ponto do r2
	double qx = x2 - nx*r2;
	double qy = y2 - ny*r2;
	double qz = z2 - nz*r2;
	
	double dx = qx-px;
	double dy = qy-py;
	double dz = qz-pz;
	
	result = sqrt( dx*dx + dy*dy  + dz*dz ) / (2*r1);
	
  //       cout << "r1 + r2 = " << r1 + r2 << endl;
  //       cout << "nx, ny = " << nx << ", " << ny << endl;
  //       cout << "px, py = " << px << ", " << py << endl;
  //       cout << "qx, qy = " << qx << ", " << qy << endl;
  //       cout << "dx, dy = " << dx << ", " << dy << endl;
  //       cout << "result = " << result << endl;
	
	if( result > 1.0 )
	  result = 1.0;
      }
      
      return result;
    }  

    bool collided( double x_, double y_, double z_ )
    {
      return( this->distance(Circle(x_,y_,z_)) <= r );
    }
    
  };  

} // namespace coro



#endif  // OBJ_BASE_H 
