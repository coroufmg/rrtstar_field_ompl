/* Copyright 2014 Guilherme Pereira
 * rrtstar_field.cpp
 *
 *  Created on: Dec 09, 2015
 *      Author: Guilherme Pereira
 */

#ifndef SAMPLING_PLANNERS_INCLUDE_SAMPLING_PLANNERS_RRTSTAR_FIELD_H_
#define SAMPLING_PLANNERS_INCLUDE_SAMPLING_PLANNERS_RRTSTAR_FIELD_H_

#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/objectives/StateCostIntegralObjective.h"
#include "ompl/geometric/planners/rrt/RRTstar.h"
#include <Eigen/Core>
#include "obj_base.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;

namespace ca {
   namespace rrtstar_field {

	// Class to define the new cost function
	class FollowVectorField : public ob::StateCostIntegralObjective 
	{
	  public:
		unsigned int dim; // dimension of the StateSpace
		
    		FollowVectorField(const ob::SpaceInformationPtr &si) : 
			ob::StateCostIntegralObjective(si, true) 
			{
			    dim = si_->getStateDimension();
			}
       
    		virtual ob::Cost motionCost(const ob::State *s1, const ob::State *s2) const; 
		
		og::PathGeometric integrateField(const ob::State *start, double delta, double max_path_size); 
    
    		virtual bool isSymmetric () const
		    	{
	   		   return false;
			}
    
    		virtual coro::vec field(const ob::State *state) const
			{
			    coro::vec v(1, 1);      // Simple 45 degrees, constant field
			    v.normalize();
  	   		    return v;
			}
	
		virtual coro::vec3 field3(const ob::State *state) const
			{
	   		    coro::vec3 v(1, 1, 1);
			    v.normalize();
  	   		    return v;	  
			}
			
		
	  }; // class FollowVectorField

	// Class to define the new cost function in 3D
	class FollowVectorField3 : public ob::StateCostIntegralObjective 
	{
	  public:
		unsigned int dim; // dimension of the StateSpace
		const std::vector<coro::Circle> *m_circles; // lista de circulos
		
    		FollowVectorField3(const ob::SpaceInformationPtr &si, const std::vector<coro::Circle> *circles = NULL) : m_circles(circles),
			ob::StateCostIntegralObjective(si, true) 
			{
			    dim = si_->getStateDimension();
			}
       
    		virtual ob::Cost motionCost(const ob::State *s1, const ob::State *s2) const; 
		
		og::PathGeometric integrateField(const ob::State *start, double delta, double max_path_size, bool collisionTest = true); 
    
    		virtual bool isSymmetric () const
		    	{
	   		   return false;
			}
    
		virtual coro::vec3 field(const ob::State *state) const
			{
	   		    coro::vec3 v(1, 1, 1);
			    v.normalize();
  	   		    return v;	  
			}	
	  }; // class FollowVectorField3


	// Class RRTstarField created to allow the redefinition of getPlannerData 
	// The new getPlannerData allows solutions without a pre-specified goal.
	// We recompute the path inside getPlannerData. The original path is still computed 
	// inside the solve() method. Therefore, getPlannerData must run before getting the 
	// path, otherwise we get the old path, which considers the goal.
	class RRTstarField : public og::RRTstar
	{

  	  double ball_radius;
	  double delta_radius;
	  double closeness_ratio;
	  mutable bool foundPath_;

	  public:
	
    		RRTstarField(const ob::SpaceInformationPtr &si) : 
		          og::RRTstar(si) {foundPath_=false;}    
 
		bool computeFinalPath() const;
		
		bool foundPath() const
			{
			  return foundPath_;
			}
		
		void setBallRadius(double radius, double delta, double acceptable_closeness_to_the_border = 0.5) 
			{ 
		  	  ball_radius=radius;
   		  	  delta_radius=delta;
			  closeness_ratio=acceptable_closeness_to_the_border;
			}
				
	  protected:
	    
	      void setFoundPath() const
		      {
			foundPath_=true;
		      }
	      void resetFoundPath() const
		      {
			foundPath_=false;
		      }
	      
	};  //class RRTstarField


	// This class was created to allow the redefinition of sampleUniform.
	// The new sampleUniform samples inside a ball.
	class RealVectorStateSamplerCircular : public ob::RealVectorStateSampler
   	{
      	   public:
	
           	RealVectorStateSamplerCircular(const ob::StateSpace *space) : 
		         ob::RealVectorStateSampler(space) {}
    
            	virtual void sampleUniform(ob::State *state);     
   	}; // class RealVectorStateSamplerCircular

	// This class was created to allow the redefinition of sampleUniform.
	// The new sampleUniform samples inside a ball.
	class DubinsStateSamplerCircular : public ob::StateSampler
   	{
      	   public:
	
           	DubinsStateSamplerCircular(const ob::StateSpace *space) : 
		         ob::StateSampler(space) {}
    
            	virtual void sampleUniform(ob::State *state);     
   	}; // class DubinsStateSamplerCircular


	// This class was created to allow the redefinition of sampleUniform.
	// The new sampleUniform samples inside a ball.
	class RealVectorStateSamplerCircular3 : public ob::RealVectorStateSampler
   	{
      	   public:
	
           	RealVectorStateSamplerCircular3(const ob::StateSpace *space) : 
		         ob::RealVectorStateSampler(space) {}
    
            	virtual void sampleUniform(ob::State *state);     
   	}; // class RealVectorStateSamplerCircular3

	// This class was created to allow the redefinition of sampleUniform.
	// The new sampleUniform samples inside a ball.
	class DubinsStateSamplerCircular3 : public ob::StateSampler
   	{
      	   public:
	
           	DubinsStateSamplerCircular3(const ob::StateSpace *space) : 
		         ob::StateSampler(space) {}
    
            	virtual void sampleUniform(ob::State *state);     
   	}; // class DubinsStateSamplerCircular3
   	
	// This class was created to allow the redefinition of allocDefaultStateSampler.
	// The new allocDefaultStateSamples will be calling the new sampling function that samples inside a ball.
	class RealVectorStateSpaceField : public ob::RealVectorStateSpace
    	{
           double ball_radius;
           ob::RealVectorStateSpace::StateType *startState;

           public:
		            	
		RealVectorStateSpaceField(unsigned int dim = 0, double radius=10.0) : ob::RealVectorStateSpace(dim), ball_radius(radius)
			{startState=NULL;}
		
		virtual ob::StateSamplerPtr allocDefaultStateSampler() const;
		
		ob::RealVectorStateSpace::StateType* getStart() const
			{	
                           if (startState == NULL)
				OMPL_INFORM("startState in RealVectorStateSpaceField was not defined!"); 
			   return startState;
			}

		double getBallRadius() const
			{
			   return ball_radius;
			}

		void setStart(const ob::ScopedState<> &start)
			{	
			   OMPL_INFORM("Defining Start!"); 	
			   startState = (ob::RealVectorStateSpace::StateType*)start.get();
			}

		void setBallRadius(double radius) 
	      		{ 
	     	  	   ball_radius=radius;
	     		}	
					
   	}; // class RealVectorStateSpaceField



   	// This class was created to allow the redefinition of allocDefaultStateSampler.
	// The new allocDefaultStateSamples will be calling the new sampling function that samples inside a ball.
	class DubinsStateSpaceField : public ob::DubinsStateSpace
    	{
           double ball_radius;
           ob::DubinsStateSpace::StateType *startState;

           public:
		            	
		DubinsStateSpaceField(unsigned int dim = 0, double radius=10.0) : ob::DubinsStateSpace(dim), ball_radius(radius)
			{startState=NULL;}
		
		virtual ob::StateSamplerPtr allocDefaultStateSampler() const;
		
		ob::DubinsStateSpace::StateType* getStart() const
			{	
                           if (startState == NULL)
				OMPL_INFORM("startState in DubinsStateSpaceField was not defined!"); 
			   return startState;
			}

		double getBallRadius() const
			{
			   return ball_radius;
			}

		void setStart(const ob::ScopedState<> &start)
			{	
			   OMPL_INFORM("Defining Start!"); 	
			   startState = (ob::DubinsStateSpace::StateType*)start.get();
			}

		void setBallRadius(double radius) 
	      		{ 
	     	  	   ball_radius=radius;
	     		}	
					
   	}; // class DubinsStateSpaceField

   } // namespace rrtstar_field
} // namespace ca



#endif  // SAMPLING_PLANNERS_INCLUDE_SAMPLING_PLANNERS_RRTSTAR_FIELD_H_ 
