#ifndef CIRCLES_3D_H
#define CIRCLES_3D_H


#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
//#include "ompl_rviz/ompl_rviz.h"
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_msgs/Bool.h>

#include "obj_base.h"

namespace coro
{

  class circles_3d
  {
  private:
    ros::NodeHandle m_nh;

    ros::Subscriber m_scan_sub;
    ros::Subscriber m_new_scan3d_sub;
    sensor_msgs::LaserScan m_scan_pts;

    ros::Publisher m_vis_pub;
    //ros::Publisher m_scan_event;
    ros::Publisher m_circles_pub;
    ros::Publisher m_laser_height_pub;
    //visualization_msgs::MarkerArray m_visCircles;
    tf::TransformListener m_listener;
    
    bool m_debug;
    bool m_gotScan;
    double m_minDist;
    double m_maxDist;
    double m_stateRadius;  
    
    double m_top_heigth_dist;
    double m_down_heigth_dist;
    int m_num_seq_laser_points;
    double m_intersection_limit;
    double m_history_duration;
    double m_max_diff;
    
    bool m_secure_enable;
    double m_secure_minDist;  
    double m_secure_initDist;
    double m_secure_liveTime;  
    ros::Time m_lastScanData;
    
    geometry_msgs::PoseStamped m_drone_pose;
    double m_droneAngle;
    double m_visionLimit;  
    
    coro::vec3 m_src_point;
    coro::vec3 m_dst_point;
    // funnel_angle - Angulo de abertura do funil.
    double m_funnel_angle;  
    double m_laser_height;

    
    std::vector<Circle> m_circles;
    
    void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in);
    void newScan3d ( const std_msgs::Bool::ConstPtr& new_scan );
    
    Circle getCircle( int iStart, int iEnd ) const;
    Circle calcPosition( int index ) const;
  // Circle testCollision( double x, double y );
    void pubVisCircles( const std::vector<Circle> &circles, double lifetime ) const;
    
    // Testa interseção do circulo interseção do 'circle' com o vertor 'm_circles'.
    // Retorna true se o circulo tiver uma interseção abaixo do limite.
    bool intersectionTest(Circle circle);
    
    // Remove circulos do historico maior que um limite de tempo.
    void clearOldCircles();
    
    void adjustVel();
    
    bool testFunnel(double *drone_state) const;
    
    double getDistance( double angle ) const;

    /*void updateCircles()
    {
      m_circles = getObstacleCircles;
    }*/

  public:
    circles_3d( ros::NodeHandle &nh,
	    std::string topic_name = "/scan" );

    // Testa se o laser está ligado e se não está muito perto de um obstaculo.
    bool isScanOk();

    std::vector<Circle> getObstacleCircles();
    const std::vector<Circle> *getCircles(){ return &m_circles; }
    
    void pubVisCircles( double lifetime = 1.0 )
    {
      pubVisCircles(m_circles, lifetime);
    }
    
    void setStateRadius( double r )
    {
      m_stateRadius = r;
    }
    
    void setMinDist( double minDist )
    {
      m_minDist = minDist;
    }
    
    void setMaxDist( double maxDist )
    {
      m_maxDist = maxDist;
    }

    void setDronePose( const geometry_msgs::PoseStamped &drone_pose )
    {
      m_drone_pose = drone_pose;
      m_droneAngle = toEulerAngle(drone_pose.pose.orientation).z;
      if(m_debug)
	ROS_INFO( "circles_3d::setDronePose: m_droneAngle = %f", m_droneAngle*180.0/M_PI );    
      
      m_src_point.x = drone_pose.pose.position.x;
      m_src_point.y = drone_pose.pose.position.y;
      m_src_point.z = drone_pose.pose.position.z;
    }
    
    void setDstPoint( const coro::vec3 &dst_point )
    {
      m_dst_point = dst_point;
    }
    
    void setFunnelAngle(double funnel_angle)
    {
      m_funnel_angle = funnel_angle;
    }
    
    void setVisionLimit( double visionLimit )
    {
      m_visionLimit = visionLimit;
    }
    
    geometry_msgs::Point toEulerAngle(geometry_msgs::Quaternion quat)
    {
      geometry_msgs::Point ans;

      tf::Matrix3x3 R_FLU2ENU(tf::Quaternion(quat.x, quat.y, quat.z, quat.w));
      R_FLU2ENU.getRPY(ans.x, ans.y, ans.z);
      return ans;
    }  
  };

} // namespace coro

#endif // CIRCLES_3D_H
