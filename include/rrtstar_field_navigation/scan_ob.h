#ifndef SCAN_OB_H
#define SCAN_OB_H


#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include "ompl_rviz/ompl_rviz.h"
#include <tf/transform_listener.h>
//#include <ompl/base/StateValidityChecker.h>
#include <geometry_msgs/PoseStamped.h>


class Circle
{
public:
  double x, y, z, r;
  ros::Time time_stamp;
  
  Circle( double x_=0, double y_=0, double z_=0, double r_=0 ): x(x_), y(y_), z(z_), r(r_) {}
  
  double distance( const Circle &c2 ) const
  {
    double dx = c2.x-x;
    double dy = c2.y-y;
    double dz = c2.z-z;
    return sqrt( dx*dx + dy*dy + dz*dz);
  }
  
  double intersection( const Circle &c2 ) const
  {
    double result = 0.0;
    double dist = distance(c2);
    double x1, y1, r1, x2, y2, r2;

    // Define o circulo 1 menor que o 2
    if( r <  c2.r ) 
    {
      x1 = x;    y1 = y;    r1 = r;
      x2 = c2.x; y2 = c2.y; r2 = c2.r;
      //cout << "c1 menor" << endl;
    }
    else
    {
      x2 = x;    y2 = y;    r2 = r;
      x1 = c2.x; y1 = c2.y; r1 = c2.r;
      //cout << "c2 menor" << endl;
    }
    //cout << "dist = " << dist << endl;
    
    // Se um circulo está em cima dou outro a intersecao é de 100%
    if( dist < 0.0001 ) 
      return 1.0;
    
    if( dist < r1 + r2 ) // Existe interseção
    {
      // direção
      double nx = (x2-x1)/dist;
      double ny = (y2-y1)/dist;	
      
      // ponto do r1
      double px = x1 + nx*r1;
      double py = y1 + ny*r1;

      // ponto do r2
      double qx = x2 - nx*r2;
      double qy = y2 - ny*r2;
      
      double dx = qx-px;
      double dy = qy-py;
      
      result = sqrt( dx*dx + dy*dy ) / (2*r1);
      
//       cout << "r1 + r2 = " << r1 + r2 << endl;
//       cout << "nx, ny = " << nx << ", " << ny << endl;
//       cout << "px, py = " << px << ", " << py << endl;
//       cout << "qx, qy = " << qx << ", " << qy << endl;
//       cout << "dx, dy = " << dx << ", " << dy << endl;
//       cout << "result = " << result << endl;
      
      if( result > 1.0 )
	result = 1.0;
    }
    
    return result;
  }  

  bool collided( double x_, double y_, double z_ )
  {
    return( this->distance(Circle(x_,y_,z_)) <= r );
  }
};

class scan_ob : public ompl::base::StateValidityChecker
{
private:
  ros::NodeHandle m_nh;

  ros::Subscriber m_scan_sub;
  sensor_msgs::LaserScan m_scan_pts;

  ros::Publisher m_vis_pub;
  //visualization_msgs::MarkerArray m_visCircles;
  
  bool m_debug;
  bool m_gotScan;
  double m_minDist;
  double m_maxDist;
  double m_stateRadius;  
  
  double m_top_heigth_dist;
  double m_down_heigth_dist;
  int m_num_seq_laser_points;
  double m_intersection_limit;
  double m_history_duration;
  double m_max_diff;
  
  bool m_secure_enable;
  double m_secure_minDist;  
  double m_secure_initDist;
  double m_secure_liveTime;  
  ros::Time m_lastScanData;
  
  geometry_msgs::PoseStamped m_drone_pose;
  double m_droneAngle;
  double m_visionLimit;  

  
  std::vector<Circle> m_circles;
  
  void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in);
  
  Circle getCircle( int iStart, int iEnd ) const;
  Circle calcPosition( int index ) const;
 // Circle testCollision( double x, double y );
  void pubVisCircles( const std::vector<Circle> &circles, double lifetime ) const;

  //double clearanceCalc(const ompl::base::State* state, int &indexNearestCircle) const;
  double clearanceCalc(Circle state, int &indexNearestCircle) const;
  Circle bestValidState( int indexCircle, const Circle &state  ) const;
  
  // Testa interseção do circulo interseção do 'circle' com o vertor 'm_circles'.
  // Retorna true se o circulo tiver uma interseção abaixo do limite.
  bool intersectionTest(Circle circle);
  
  // Remove circulos do historico maior que um limite de tempo.
  void clearOldCircles();
  
  void adjustVel();

  /*void updateCircles()
  {
    m_circles = getObstacleCircles;
  }*/

public:
  scan_ob( const ompl::base::SpaceInformationPtr& si, ros::NodeHandle &nh,
	   std::string topic_name = "/scan", bool debug = true );

  // Testa se o laser está ligado e se não está muito perto de um obstaculo.
  bool isScanOk();
  
  bool isValid(const ompl::base::State* state) const;
  double clearance(const ompl::base::State* state) const;
  //double bestValidState(const ompl::base::State* state, int &indexNearestCircle) const;
  double getBestValidState(const ompl::base::State* state) const;
  
  std::vector<Circle> getObstacleCircles(tf::TransformListener &listener);
  
  void pubVisCircles(double lifetime = 1.0)
  {
    pubVisCircles(m_circles,lifetime);
  }
  
  void setStateRadius( double r )
  {
    m_stateRadius = r;
  }
  
  void setMinDist( double minDist )
  {
    m_minDist = minDist;
  }
  
  void setMaxDist( double maxDist )
  {
    m_maxDist = maxDist;
  }

  void setDronePose( const geometry_msgs::PoseStamped &drone_pose )
  {
    m_drone_pose = drone_pose;
    m_droneAngle = toEulerAngle(drone_pose.pose.orientation).z;
    if(m_debug)
      ROS_INFO( "scan_ob::setDronePose: m_droneAngle = %f", m_droneAngle*180.0/M_PI );    
  }
  
  void setVisionLimit( double visionLimit )
  {
    m_visionLimit = visionLimit;
  }
  
  geometry_msgs::Point toEulerAngle(geometry_msgs::Quaternion quat)
  {
    geometry_msgs::Point ans;

    tf::Matrix3x3 R_FLU2ENU(tf::Quaternion(quat.x, quat.y, quat.z, quat.w));
    R_FLU2ENU.getRPY(ans.x, ans.y, ans.z);
    return ans;
  }  
};

#endif // SCAN_OB_H
