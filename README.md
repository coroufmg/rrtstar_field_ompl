### What is this repository for? ###

* This software implements the integration of RRT* and Vector Fields for robot navigation. The difference from the [previous package](https://bitbucket.org/coroufmg/rrtstar_field) is that it uses most recent version of OMPL. It also do not require other packages other than OMPL and ompl_rviz to visualize the results.

### How do I get set up? ###


Create a workspace: 

```
#!bash

source /opt/ros/indigo/setup.bash # this is usually not necessary since it is done inside .bashrc 
mkdir -p ~/rrtstar_field_ws/src
cd rrtstar_field_ws/src
catkin_init_workspace

```

Download the package:



```
#!bash
git clone git@bitbucket.org:coroufmg/rrtstar_field_ompl.git

```

Install dependencies: 

1 - ompl_rviz -- necessary to visualize paths and graphs in rviz

```
#!bash

git clone git@bitbucket.org:coroufmg/ompl_rviz.git

```

2 - ompl -- The main Open Motion Planning Library
 
See the instruction at [the library web site](http://ompl.kavrakilab.org/download.html).

Compile:


```
#!bash

cd ..
catkin_make
```

Run an example:

```
#!bash

source devel/setup.bash
roslaunch rrtstar_field_navigation rrtstar_field.launch 

```


### Who do I talk to? ###

* gpereira@ufmg.br