#!/bin/bash

get_dir_name()
{
    if [ -d $1 ]
    then
	get_dir_name $1'1'
    else
	dir_name=$1
    fi
}

file="*.bag*"
count=`ls -1 $file 2>/dev/null | wc -l`
if [ $count != 0 ]
then
	get_dir_name bg_
	mkdir $dir_name
	mv *.bag* $dir_name
	echo "$file found. $dir_name created and moved files."
else
	echo "$file not found."
fi




